package fsm.com.piscis.domain;

import java.util.List;

import fsm.com.piscis.model.Fish;
import fsm.com.piscis.model.KinshipLevel;
import fsm.com.piscis.model.Tank;
import fsm.com.piscis.model.User;
import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {

    //---- Fishes
    @GET("fishes")
    Single<List<Fish>> getAllFishes();

    @GET("fish/find")
    Single<List<Fish>> findFish(@Query("fish_info") String fishInfo);

    @GET("fish/from_tank")
    Single<List<Fish>> getFishesFromTank(@Query("tank_id") String tankId);

    @POST("fish/new")
    Single<Fish> createFish(@Body Fish fish);

    @PUT("fish/edit")
    Single<Fish> editFish(@Body Fish fish);

    @FormUrlEncoded
    @POST("fish/discard")
    Completable discardFish(@Field("fish_id") String fish_id);

    @GET("fish/out_tank")
    Single<List<Fish>> getFishesOutTank(@Query("tank_id") String tankId);

    @FormUrlEncoded
    @POST("fish/add_combination")
    Single<Response<KinshipLevel>> addGeneticCombination(@Field("tag1") String tag1,
                                                         @Field("tag2") String tag2,
                                                         @Field("level") int level);

    @GET("api/fish/combination")
    Single<Response<KinshipLevel>> getGeneticMatch(@Query("tag1") String tag1,
                                                   @Query("tag2") String tag2);

    @GET("fish/relatives_for_fish")
    Single<List<KinshipLevel>> getKinsForFish(@Query("fish_id") String fishId);


    //---- Tanks
    @GET("tanks")
    Single<List<Tank>> getAllTanks();

    @POST("tank/new")
    Single<Tank> createTank(@Body Tank tank);

    @PUT("tank/edit")
    Single<Tank> editTank(@Body Tank tank);

    @DELETE("tank/delete/{tank_id}")
    Single<Response<Void>> removeTank(@Path("tank_id") String tankId);

    @FormUrlEncoded
    @POST("tank/add_fish")
    Single<Tank> addFishToTank(@Field("fish_id") String fishId,
                               @Field("tank_id") String tankId);

    @POST("tank/add_fishes")
    Single<Tank> addFishesToTank(@Body Tank tank);

    @FormUrlEncoded
    @POST("tank/remove_fish")
    Single<Tank> removeFishFromTank(@Field("fish_id") String fishId);


    //---- Users
    @POST("users/sign-up")
    Single<User> signUp(@Body User user);

    @FormUrlEncoded
    @POST("/api/login")
    Single<User> login(@Field("username") String username,
                       @Field("password") String password);

    @GET("users")
    Single<List<User>> getUsers();

    @FormUrlEncoded
    @POST("user/change_password")
    Completable changePassword(@Field("user_id") String userId,
                               @Field("old_pwd") String oldPwd,
                               @Field("new_pwd") String newPwd);

    @FormUrlEncoded
    @POST("user/change_email")
    Single<User> changeEmail(@Field("user_id") String userId,
                             @Field("old_pwd") String oldPwd,
                             @Field("email") String email);

    @DELETE("user/delete/{user_id}")
    Single<Response<Void>> removeUsers(@Path("user_id") String userId);
}
