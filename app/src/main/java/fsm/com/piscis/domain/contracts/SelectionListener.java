package fsm.com.piscis.domain.contracts;

import fsm.com.piscis.model.Fish;

public interface SelectionListener {
    void selectedFish(Fish fish);
}
