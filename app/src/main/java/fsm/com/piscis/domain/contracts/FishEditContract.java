package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import fsm.com.piscis.model.Fish;

public interface FishEditContract {
    interface View extends MvpView {
        void showProgress();

        void hideProgress();

        void fishEditSuccess();

        void showServerError();

        void showError(String message);

        void logout();
    }

    interface Presenter extends MvpPresenter<View> {
        void editFish(Fish fish);
    }
}
