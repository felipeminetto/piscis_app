package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.Date;

import fsm.com.piscis.model.Fish;

public interface FishDetailContract {
    interface Presenter {
        void discardFish(Fish fish);

        void addReproductionDate(Date d, Fish fish);
    }

    interface View extends MvpView {
        void showProgress();

        void hideProgress();

        void showServerError();

        void showError(String message);

        void logout();

        void showSuccess(String message);

        void updateFish(Fish fish);
    }
}
