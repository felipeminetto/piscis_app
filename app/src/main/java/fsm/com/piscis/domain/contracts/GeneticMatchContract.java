package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import fsm.com.piscis.model.KinshipLevel;

public interface GeneticMatchContract {
    interface View extends MvpView {
        void showProgress();

        void hideProgress();

        void createdMatch(KinshipLevel match);

        void errorCreatingMatch(String message);

        void matchFound(KinshipLevel match);

        void matchNotFound(String message);

        void showServerError();

        void showError(String message);

        void logout();
    }

    interface Presenter extends MvpPresenter<View> {
        void createMatch(String tag1, String tag2, int level);

        void findMatch(String tag1, String tag2);
    }
}
