package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import fsm.com.piscis.model.Fish;
import fsm.com.piscis.model.Tank;

public interface TankDetailContract {

    interface View extends MvpView {
        void showProgress();

        void hideProgress();

        void showFishes(List<Fish> fishes);

        void showServerError();

        void showError(String message);

        void logout();

        void deletedTank();

        void deleteTankError();
    }

    interface Presenter extends MvpPresenter<View> {
        void loadFishesFromTank(String tankId);

        void deleteTank(Tank tank);
    }
}
