package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import fsm.com.piscis.model.User;

public interface UsersContract {

    interface View extends MvpView {
        void showUsers(List<User> userList);

        void showServerError();

        void showError(String message);

        void logout();

        void showProgress();

        void hideProgress();
    }

    interface Presenter extends MvpPresenter<View> {
        void loadAllUsers();

        void removeUser(User user);
    }

    interface UserSelectionListener {
        void removeUser(User user);
    }
}
