package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import fsm.com.piscis.model.User;

public interface ProfileContract {
    interface View extends MvpView {
        void emailChanged(User user1);

        void showServerError();

        void showError(String message);

        void logout();

        void passowrdChanged();
    }

    interface Presenter extends MvpPresenter<View> {
        void updateUserEmail(User user, String pwd, String email);

        void changePassword(User user, String oldPwd, String newPwd, String confPwd);

        void logout();
    }
}
