package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import fsm.com.piscis.model.Tank;

public interface TankContract {

    interface View extends MvpView {
        void showTanks(List<Tank> tanks);

        void showProgress();

        void hideProgress();

        void showServerError();

        void showError(String message);

        void logout();
    }

    interface Presenter extends MvpPresenter<View> {
        void loadTanks();
    }
}
