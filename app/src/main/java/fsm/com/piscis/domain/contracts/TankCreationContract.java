package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import fsm.com.piscis.model.Tank;

public interface TankCreationContract {
    interface View extends MvpView {
        void tankCreated(Tank tank);

        void showServerError();

        void showError(String message);

        void logout();
    }

    interface Presenter extends MvpPresenter<View> {
        void createTank(Tank tank);
    }
}
