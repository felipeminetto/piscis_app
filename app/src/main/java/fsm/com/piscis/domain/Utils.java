package fsm.com.piscis.domain;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.HashMap;

import fsm.com.piscis.base.Constants;
import fsm.com.piscis.model.User;
import retrofit2.HttpException;

import static android.content.Context.MODE_PRIVATE;

public class Utils {
    public static String getErrorMessage(Throwable throwable) {
        try {
            Gson gson = new Gson();
            HashMap<String, String> map = null;
            map = gson.fromJson(((HttpException) throwable).response().errorBody().string(), HashMap.class);
            return map.get("message");
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static User getUser(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
        String userJson = prefs.getString(Constants.USER, "");
        return new Gson().fromJson(userJson, User.class);
    }
}
