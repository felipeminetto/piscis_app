package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import fsm.com.piscis.model.Fish;

public interface FishCreationContract {

    interface View extends MvpView {
        void showTagWarning(Fish fish);

        void fishSaved();

        void showTagError(Fish fish, String message);

        void showServerError();

        void showError(String message);

        void logout();
    }

    interface Presenter extends MvpPresenter<View> {
        void saveFish(Fish fish);

        void createFish(Fish fish, boolean discardAndSave);
    }
}
