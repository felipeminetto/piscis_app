package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import fsm.com.piscis.model.KinshipLevel;

public interface ReprodutiveDirectionContract {

    interface View extends MvpView {
        void showProgress();

        void hideProgress();

        void showMatchFishes(List<KinshipLevel> matches);

        void showServerError();
    }

    interface Presenter extends MvpPresenter<View> {
        void loadMatchesForFish(String fishId);
    }
}
