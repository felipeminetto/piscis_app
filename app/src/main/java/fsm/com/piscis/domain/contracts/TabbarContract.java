package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface TabbarContract {

    interface View extends MvpView {
        void navigateToFishes();

        void navigateToTanks();

        void navigateToUsers();
    }

    interface Presenter extends MvpPresenter<View> {

    }
}
