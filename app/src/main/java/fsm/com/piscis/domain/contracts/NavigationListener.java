package fsm.com.piscis.domain.contracts;

import android.support.v4.app.Fragment;

import fsm.com.piscis.model.Fish;

public interface NavigationListener {

    void addFragmentToNavigation(Fragment fragment, String name);

    void finishFragment();

    void navigateTo(int fragment);

    void finishAndUpdateFish(Fish fish);
}
