package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface LoginContract {
    interface View extends MvpView {

        void startApp();

        void loginSuccess();

        void loginFail();

        void invalidLogin();
    }

    interface Presenter extends MvpPresenter<View> {
        void login(String username, String password);
    }
}
