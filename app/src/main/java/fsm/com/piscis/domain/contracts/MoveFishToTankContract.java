package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import fsm.com.piscis.model.Fish;
import fsm.com.piscis.model.Tank;

public interface MoveFishToTankContract {
    interface View extends MvpView {

        void showTanks(List<Tank> tanks);

        void showProgress();

        void hideProgress();

        void showServerError();

        void showError(String message);

        void logout();

        void fishAddedToTank(Fish fish);

    }

    interface Presenter {

        void moveFishToTank(Fish fish, Tank tank);

        void loadTanks();
    }
}
