package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import fsm.com.piscis.model.Fish;
import fsm.com.piscis.model.Tank;

public interface AddFishToTankContract {

    interface View extends MvpView {

        void showServerError();

        void fishesAdded();

        void showProgress();

        void hideProgress();

        void showFishes(List<Fish> fishes);

        void showError(String message);

        void logout();
    }

    interface Presenter extends MvpPresenter<View> {

        void addFishesToTank(List<Fish> fishes, Tank tank);

        void loadFishesOutTank(Tank tank);
    }
}
