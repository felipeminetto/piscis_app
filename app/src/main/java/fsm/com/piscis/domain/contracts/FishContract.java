package fsm.com.piscis.domain.contracts;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import fsm.com.piscis.model.Fish;

public interface FishContract {

    interface View extends MvpView {
        void showFishes(List<Fish> fishes);

        void showProgress();

        void hideProgress();

        void showFishDetail(Fish fish);

        void showServerError();

        void showError(String message);

        void logout();
    }

    interface Presenter extends MvpPresenter<View> {
        void loadFishes();

        void findFish(String fishInfo);
    }
}
