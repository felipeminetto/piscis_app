package fsm.com.piscis.domain.contracts;

import fsm.com.piscis.model.Tank;

public interface TankSelectionListener {
    void selectedTank(Tank tank);
}
