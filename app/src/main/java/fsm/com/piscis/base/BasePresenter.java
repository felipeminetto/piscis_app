package fsm.com.piscis.base;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import fsm.com.piscis.domain.APIService;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BasePresenter<T extends MvpView> extends MvpBasePresenter<T> {

    protected APIService apiService;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public BasePresenter(APIService apiService) {
        this.apiService = apiService;
    }

    @Override
    public void detachView() {
        compositeDisposable.clear();
        super.detachView();
    }

    public void addSubscription(Disposable subscription) {
        compositeDisposable.add(subscription);
    }

}
