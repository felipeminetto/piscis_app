package fsm.com.piscis.base;

public class Constants {

    public static final String PREF_NAME = "Piscis_Preferences";
    public static final String AUTHORIZATION = "Authorization";
    public static final String ADD_FISH = "fish_creation";
    public static final String ADD_TANK = "tank_creation";
    public static final String FISH_DETAIL = "fish_detail";
    public static final String ADD_FISH_TO_TANK = "fishes_to_tank";
    public static final String USER = "user";
    public static final String FISH = "fish";
    public static final String REPROD_DIRECT = "reprod_drection";
    public static final String TANK = "tank";
    public static final String EDIT_FISH = "edit_fish";
    public static final String MOVE_FISH = "move_fish";
    public static final String USERS = "users";
}
