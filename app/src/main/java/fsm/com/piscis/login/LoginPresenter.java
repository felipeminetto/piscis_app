package fsm.com.piscis.login;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import fsm.com.piscis.base.BasePresenter;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.domain.APIService;
import fsm.com.piscis.domain.contracts.LoginContract;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class LoginPresenter extends BasePresenter<LoginContract.View> implements LoginContract.Presenter {

    private SharedPreferences preferences;

    public LoginPresenter(APIService service, SharedPreferences preferences) {
        super(service);
        this.preferences = preferences;
    }

    @Override
    public void login(String username, String password) {
        addSubscription(apiService.login(username, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                            String authorization = user.getToken();
                            Gson gson = new Gson();
                            String userJson = gson.toJson(user);
                            preferences.edit()
                                    .putString(Constants.USER, userJson)
                                    .apply();
                            if (authorization != null && !authorization.isEmpty()) {
                                saveAuthorizationHeader(authorization);
                                ifViewAttached(LoginContract.View::loginSuccess);
                            } else {
                                ifViewAttached(LoginContract.View::invalidLogin);
                            }
                        },
                        throwable -> {
                            throwable.printStackTrace();
                            if (throwable instanceof HttpException) {
                                if (((HttpException) throwable).code() == 403) {
                                    ifViewAttached(LoginContract.View::invalidLogin);
                                }
                            }
                            ifViewAttached(LoginContract.View::loginFail);
                        }));
    }

    private void saveAuthorizationHeader(String authorization) {
        preferences.edit()
                .putString(Constants.AUTHORIZATION, authorization)
                .apply();
    }
}
