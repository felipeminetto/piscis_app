package fsm.com.piscis.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import fsm.com.piscis.PiscisApplication;
import fsm.com.piscis.R;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.databinding.ActivityLoginBinding;
import fsm.com.piscis.domain.contracts.LoginContract;
import fsm.com.piscis.tabbar.TabbarActivity;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    private ActivityLoginBinding binding;
    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        SharedPreferences prefs = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
        String auth = prefs.getString(Constants.AUTHORIZATION, "");
        if (!auth.isEmpty()) {
            startApp();
        }
        binding.setActivity(this);
        presenter = new LoginPresenter(((PiscisApplication) getApplication()).getApi(), prefs);
        presenter.attachView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle("Login");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    public void makeLogin() {
        presenter.login(binding.edtEmail.getText().toString(), binding.edtPasswd.getText().toString());
    }

    @Override
    public void startApp() {
        Intent intent = new Intent(this, TabbarActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void loginSuccess() {
        startApp();
    }

    @Override
    public void loginFail() {
        Toast.makeText(this, "Credenciais Inválidas!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void invalidLogin() {
        Toast.makeText(this, "Credenciais Inválidas!", Toast.LENGTH_SHORT).show();
    }
}
