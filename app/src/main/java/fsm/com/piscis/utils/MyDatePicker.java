package fsm.com.piscis.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

public class MyDatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    DateSelectedListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        return datePickerDialog;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        listener.dateSelected(day, month, year);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        listener.onPickerCanceled();
        super.onCancel(dialog);
    }

    public void setListener(DateSelectedListener listener) {
        this.listener = listener;
    }

    public interface DateSelectedListener {
        void dateSelected(int day, int month, int year);
        void onPickerCanceled();
    }
}

