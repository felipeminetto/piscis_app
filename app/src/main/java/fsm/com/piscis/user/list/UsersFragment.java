package fsm.com.piscis.user.list;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fsm.com.piscis.PiscisApplication;
import fsm.com.piscis.R;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.databinding.ListFragmentBinding;
import fsm.com.piscis.domain.contracts.NavigationListener;
import fsm.com.piscis.domain.contracts.UsersContract;
import fsm.com.piscis.model.User;
import fsm.com.piscis.login.LoginActivity;
import fsm.com.piscis.user.list.adapter.UsersAdapter;

import static android.content.Context.MODE_PRIVATE;

public class UsersFragment extends Fragment implements UsersContract.View, UsersContract.UserSelectionListener {

    private UsersPresenter presenter;
    private ListFragmentBinding binding;
    private NavigationListener listener;

    public static UsersFragment newInstance() {
        return new UsersFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false);
        presenter = new UsersPresenter(((PiscisApplication) getActivity().getApplication()).getApi());
        presenter.attachView(this);
        presenter.loadAllUsers();
        binding.swipeRefreshLayout.setOnRefreshListener(presenter::loadAllUsers);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Minha Conta");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showUsers(List<User> userList) {
        binding.swipeRefreshLayout.setRefreshing(false);
        binding.rvItems.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvItems.setAdapter(new UsersAdapter(userList, this));
        binding.executePendingBindings();
    }

    @Override
    public void showServerError() {
        showError("Houve algum erro na comunicação com o servidor, tente novamente.");
    }

    @Override
    public void showError(String message) {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .create().show();
    }

    @Override
    public void showProgress() {
        binding.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.progress.setVisibility(View.GONE);
    }

    public void setListener(NavigationListener listener) {
        this.listener = listener;
    }

    @Override
    public void removeUser(User user) {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage("Tem certeza que deseja remover o usuário " + user.getName() + "?")
                .setPositiveButton("Sim", ((dialogInterface, i) -> {
                    presenter.removeUser(user);
                }))
                .setNegativeButton("Não", null)
                .create().show();
    }

    @Override
    public void logout() {
        new android.support.v7.app.AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage("A sua secção expirou! Favor efetuar o login novamente.")
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                    SharedPreferences prefs = getContext().getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
                    prefs.edit().clear().apply();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                    getActivity().finish();
                })
                .create().show();
    }
}
