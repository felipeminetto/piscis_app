package fsm.com.piscis.user.profile;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fsm.com.piscis.PiscisApplication;
import fsm.com.piscis.R;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.databinding.ChangeEmailDialogBinding;
import fsm.com.piscis.databinding.ChangePasswordDialogBinding;
import fsm.com.piscis.databinding.UserFragmentBinding;
import fsm.com.piscis.domain.Utils;
import fsm.com.piscis.domain.contracts.NavigationListener;
import fsm.com.piscis.domain.contracts.ProfileContract;
import fsm.com.piscis.model.User;
import fsm.com.piscis.login.LoginActivity;
import fsm.com.piscis.user.list.UsersFragment;

import static android.content.Context.MODE_PRIVATE;

public class ProfileFragment extends Fragment implements ProfileContract.View {

    private UserFragmentBinding binding;

    private ProfilePresenter presenter;

    private NavigationListener listener;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.user_fragment, container, false);
        SharedPreferences prefs = getActivity().getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
        presenter = new ProfilePresenter(((PiscisApplication) getActivity().getApplication()).getApi(), prefs);
        presenter.attachView(this);
        User user = Utils.getUser(getContext());
        if (user != null) {
            binding.setUser(user);
            getActivity().setTitle(user.getName());
        }

        binding.setFragment(this);

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Minha Conta");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    public void logout() {
        new AlertDialog.Builder(getContext())
                .setTitle("Logout")
                .setMessage("Tem certeza que deseja sair do aplicativo'?")
                .setPositiveButton("Sim", ((dialogInterface, i) -> {
                    SharedPreferences prefs = getContext().getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
                    prefs.edit().clear().apply();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                    getActivity().finish();
                }))
                .setNegativeButton("Não", null)
                .create().show();
    }

    public void manageUsers() {
        UsersFragment fragment = UsersFragment.newInstance();
        fragment.setListener(listener);
        listener.addFragmentToNavigation(fragment, Constants.USERS);
    }

    public void changePwd(User user) {
        ChangePasswordDialogBinding changePwdBinding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.change_password_dialog, null, false);
        new AlertDialog.Builder(getContext())
                .setTitle("Alterar senha")
                .setView(changePwdBinding.getRoot())
                .setPositiveButton("Alterar", (dialogInterface, i) -> {
                    String oldPwd = changePwdBinding.edtOldPasswd.getText().toString();
                    String newPwd = changePwdBinding.edtNewPasswd.getText().toString();
                    String confPwd = changePwdBinding.edtConfPasswd.getText().toString();
                    presenter.changePassword(user, oldPwd, newPwd, confPwd);
                    dialogInterface.dismiss();
                })
                .setNegativeButton("Cancelar", null)
                .create().show();
    }

    public void changeEmail(User user) {
        ChangeEmailDialogBinding changePwdBinding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.change_email_dialog, null, false);
        new AlertDialog.Builder(getContext())
                .setTitle("Alterar email")
                .setView(changePwdBinding.getRoot())
                .setPositiveButton("Alterar", (dialogInterface, i) -> {
                    String email = changePwdBinding.edtNewEmail.getText().toString();
                    String pwd = changePwdBinding.edtPasswd.getText().toString();
                    presenter.updateUserEmail(user, pwd, email);
                    dialogInterface.dismiss();
                })
                .setNegativeButton("Cancelar", null)
                .create().show();
    }

    @Override
    public void emailChanged(User user) {
        binding.setUser(user);
        new AlertDialog.Builder(getContext())
                .setTitle("Sucesso!")
                .setMessage("Email alterada com sucesso!")
                .setPositiveButton(android.R.string.ok, null)
                .create().show();
    }

    @Override
    public void showServerError() {
        showError("Houve algum erro na comunicação com o servidor, tente novamente.");
    }

    @Override
    public void showError(String message) {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .create().show();
    }

    @Override
    public void passowrdChanged() {
        new AlertDialog.Builder(getContext())
                .setTitle("Sucesso!")
                .setMessage("Senha alterada com sucesso!")
                .setPositiveButton(android.R.string.ok, null)
                .create().show();
    }

    public void setListener(NavigationListener listener) {
        this.listener = listener;
    }
}
