package fsm.com.piscis.user.list.adapter;

import android.support.v7.widget.RecyclerView;

import fsm.com.piscis.databinding.UserListItemBinding;
import fsm.com.piscis.domain.contracts.UsersContract;
import fsm.com.piscis.model.User;

public class UserViewHolder extends RecyclerView.ViewHolder {

    private UserListItemBinding binding;

    public UserViewHolder(UserListItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(User user, UsersContract.UserSelectionListener listener) {
        binding.setUser(user);
        binding.setListener(listener);
    }
}
