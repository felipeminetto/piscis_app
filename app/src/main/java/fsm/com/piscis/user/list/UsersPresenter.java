package fsm.com.piscis.user.list;

import fsm.com.piscis.base.BasePresenter;
import fsm.com.piscis.domain.APIService;
import fsm.com.piscis.domain.Utils;
import fsm.com.piscis.domain.contracts.UsersContract;
import fsm.com.piscis.model.User;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class UsersPresenter extends BasePresenter<UsersContract.View> implements UsersContract.Presenter {

    public UsersPresenter(APIService service) {
        super(service);
    }

    @Override
    public void removeUser(User user) {
        ifViewAttached(UsersContract.View::showProgress);
        addSubscription(apiService.removeUsers(user.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    ifViewAttached(UsersContract.View::hideProgress);
                    if (response.isSuccessful()) {
                        ifViewAttached(UsersContract.View::hideProgress);
                        loadAllUsers();
                    } else {
                        ifViewAttached(view -> view.showError("Não foi possível remover o usuário. Verifique sua conexão com a internet e tente novamente mais tarde."));
                    }
                }, throwable -> {
                    ifViewAttached(UsersContract.View::hideProgress);
                    if (throwable instanceof HttpException) {
                        if (((HttpException) throwable).code() == 401) {
                            ifViewAttached(view -> view.logout());
                            ;
                        } else if (((HttpException) throwable).code() >= 500) {
                            ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                        } else {
                            String message = Utils.getErrorMessage(throwable);
                            ifViewAttached(view -> view.showError(message));
                        }
                    } else {
                        ifViewAttached(view -> view.showError("Não foi possível remover o usuário. Verifique sua conexão com a internet e tente novamente mais tarde."));
                    }
                }));
    }

    @Override
    public void loadAllUsers() {
        ifViewAttached(UsersContract.View::showProgress);
        addSubscription(apiService.getUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(users -> {
                    ifViewAttached(UsersContract.View::hideProgress);
                    ifViewAttached(view -> view.showUsers(users));
                }, throwable -> {
                    ifViewAttached(UsersContract.View::hideProgress);
                    if (throwable instanceof HttpException) {
                        if (((HttpException) throwable).code() == 401) {
                            ifViewAttached(view -> view.logout());
                            ;
                        } else if (((HttpException) throwable).code() >= 500) {
                            ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                        } else {
                            String message = Utils.getErrorMessage(throwable);
                            ifViewAttached(view -> view.showError(message));
                        }
                    } else {
                        ifViewAttached(view -> view.showError("Não foi possível alterar o email. Verifique sua conexão com a internet e tente novamente mais tarde."));
                    }
                }));
    }
}
