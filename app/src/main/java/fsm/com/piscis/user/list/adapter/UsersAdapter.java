package fsm.com.piscis.user.list.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import fsm.com.piscis.R;
import fsm.com.piscis.databinding.UserListItemBinding;
import fsm.com.piscis.domain.contracts.UsersContract;
import fsm.com.piscis.model.User;

public class UsersAdapter extends RecyclerView.Adapter<UserViewHolder> {

    private List<User> users;
    private UsersContract.UserSelectionListener listener;

    public UsersAdapter(List<User> users, UsersContract.UserSelectionListener listener) {
        this.users = users;
        this.listener = listener;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        UserListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.user_list_item, parent, false);
        return new UserViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        holder.bind(users.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}
