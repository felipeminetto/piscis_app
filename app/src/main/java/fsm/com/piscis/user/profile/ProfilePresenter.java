package fsm.com.piscis.user.profile;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import fsm.com.piscis.base.BasePresenter;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.domain.APIService;
import fsm.com.piscis.domain.Utils;
import fsm.com.piscis.domain.contracts.ProfileContract;
import fsm.com.piscis.model.User;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class ProfilePresenter extends BasePresenter<ProfileContract.View> implements ProfileContract.Presenter {

    private final SharedPreferences preferences;

    public ProfilePresenter(APIService apiService, SharedPreferences preferences) {
        super(apiService);
        this.preferences = preferences;
    }

    @Override
    public void updateUserEmail(User user, String pwd, String email) {
        addSubscription(apiService.changeEmail(user.getId(), pwd, email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user1 -> {
                    Gson gson = new Gson();
                    String userJson = gson.toJson(user);
                    preferences.edit()
                            .putString(Constants.USER, userJson)
                            .apply();
                    ifViewAttached(view -> view.emailChanged(user1));
                }, throwable -> {
                    if (throwable instanceof HttpException) {
                        if (((HttpException) throwable).code() == 401) {
                            ifViewAttached(view -> view.logout());
                        } else if (((HttpException) throwable).code() >= 500) {
                            ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                        } else {
                            String message = Utils.getErrorMessage(throwable);
                            ifViewAttached(view -> view.showError(message));
                        }
                    } else {
                        ifViewAttached(view -> view.showError("Não foi possível alterar o email. Verifique sua conexão com a internet e tente novamente mais tarde."));
                    }
                }));
    }

    @Override
    public void changePassword(User user, String oldPwd, String newPwd, String confPwd) {
        if (!newPwd.equals(confPwd)) {
            ifViewAttached(view -> view.showError("Confirmação de senha não é igual à senha inserida"));
        } else {
            addSubscription(apiService.changePassword(user.getId(), oldPwd, newPwd)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                        ifViewAttached(ProfileContract.View::passowrdChanged);
                    }, throwable -> {
                        if (throwable instanceof HttpException) {
                            if (((HttpException) throwable).code() == 401) {
                                ifViewAttached(view -> view.logout());
                            } else if (((HttpException) throwable).code() >= 500) {
                                ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                            } else {
                                String message = Utils.getErrorMessage(throwable);
                                ifViewAttached(view -> view.showError(message));
                            }
                        } else {
                            ifViewAttached(view -> view.showError("Não foi possível alterar a senha. Verifique sua conexão com a internet e tente novamente mais tarde."));
                        }
                    }));
        }
    }

    @Override
    public void logout() {

    }
}
