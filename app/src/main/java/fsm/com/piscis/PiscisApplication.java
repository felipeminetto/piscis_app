package fsm.com.piscis;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fsm.com.piscis.base.Constants;
import fsm.com.piscis.domain.APIService;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class PiscisApplication extends Application {

    Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public String getAuthentication() {
        SharedPreferences prefs = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
        return prefs.getString(Constants.AUTHORIZATION, "");

    }

    public Retrofit getRetrofit() {
        if (retrofit == null) {
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    .setPrettyPrinting()
                    .create();
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .addInterceptor((chain -> {
                        String auth = getAuthentication();
                        Log.d("", "using auth" + auth);
                        Request request = chain.request()
                                .newBuilder()
                                .addHeader("Authorization", "Bearer " + auth)
                                .build();

                        return chain.proceed(request);
                    }))
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }

        return retrofit;
    }

    public APIService getApi() {
        return getRetrofit().create(APIService.class);
    }
}
