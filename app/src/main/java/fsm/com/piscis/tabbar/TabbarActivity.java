package fsm.com.piscis.tabbar;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import fsm.com.piscis.R;
import fsm.com.piscis.databinding.TabbarActivityBinding;
import fsm.com.piscis.domain.contracts.NavigationListener;
import fsm.com.piscis.domain.contracts.TabbarContract;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.fish.detail.FishDetailFragment;
import fsm.com.piscis.fish.edit.FishEditFragment;
import fsm.com.piscis.fish.list.FishesListFragment;
import fsm.com.piscis.tank.list.TankListFragment;
import fsm.com.piscis.user.profile.ProfileFragment;

public class TabbarActivity extends AppCompatActivity implements TabbarContract.View, NavigationListener {

    private FragmentManager fragmentManager;

    private ActionBar actionBar;

    private int firstStateId;

    private Fragment currentFragment;

    private TabbarActivityBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.tabbar_activity);
        fragmentManager = getSupportFragmentManager();
        actionBar = getSupportActionBar();

        navigateToFishes();
        binding.bottomMenu.setOnNavigationItemSelectedListener(item -> {
            if (binding.bottomMenu.getSelectedItemId() != item.getItemId()) {
                if (currentFragment instanceof FishEditFragment) {
                    ((FishEditFragment) currentFragment).cancelEditForNavigation(item.getItemId());
                } else {
                    navigateTo(item.getItemId());
                    return true;
                }
                return false;
            }
            return false;
        });

        binding.bottomMenu.setOnNavigationItemReselectedListener(item -> {
            int count = fragmentManager.getBackStackEntryCount();
            if (count > 1)
                fragmentManager.popBackStack(firstStateId, 0);
        });

        fragmentManager.addOnBackStackChangedListener(() -> {
            int bsCount = fragmentManager.getBackStackEntryCount();
            actionBar.setDisplayHomeAsUpEnabled(bsCount > 1);

            if (bsCount > 1) {
                FragmentManager.BackStackEntry bse = fragmentManager.getBackStackEntryAt(bsCount - 1);
                currentFragment = fragmentManager.findFragmentByTag(bse.getName());
            } else {
                currentFragment = null;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (currentFragment instanceof FishEditFragment) {
            ((FishEditFragment) currentFragment).cancelEdit();
        } else {
            if (fragmentManager.getBackStackEntryCount() > 1)
                fragmentManager.popBackStack();
        }
    }

    @Override
    public void navigateToFishes() {
        FishesListFragment fragment = FishesListFragment.newInstance();
        fragment.setListener(this);
        replaceFragment(fragment);
    }

    @Override
    public void navigateToTanks() {
        TankListFragment fragment = TankListFragment.newInstance();
        fragment.setListener(this);
        replaceFragment(fragment);
    }

    @Override
    public void navigateToUsers() {
        ProfileFragment fragment = ProfileFragment.newInstance();
        fragment.setListener(this);
        replaceFragment(fragment);
    }

    @Override
    public void addFragmentToNavigation(Fragment fragment, String name) {
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment, name)
                .addToBackStack(name)
                .commit();
    }

    private void replaceFragment(Fragment fragment) {
        firstStateId = fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack("home")
                .commit();
    }

    @Override
    public void navigateTo(int menuItemId) {
        fragmentManager.popBackStack(firstStateId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        switch (menuItemId) {
            case R.id.menu_fishes:
                navigateToFishes();
                break;
            case R.id.menu_tanks:
                navigateToTanks();
                break;
//            case R.id.menu_genetic:
//                navigateToGenetic();
//                break;
            case R.id.menu_account:
                navigateToUsers();
                break;
        }
    }

    @Override
    public void finishFragment() {
        fragmentManager.popBackStack();
    }

    @Override
    public void finishAndUpdateFish(Fish fish) {
        fragmentManager.popBackStackImmediate();
        if (currentFragment instanceof FishDetailFragment) {
            ((FishDetailFragment) currentFragment).updateFish(fish);
        }
    }
}
