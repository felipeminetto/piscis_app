package fsm.com.piscis.model;

public class KinshipLevel {

    private Fish fish1;

    private Fish fish2;

    private float level;

    public Fish getFish1() {
        return fish1;
    }

    public void setFish1(Fish fish1) {
        this.fish1 = fish1;
    }

    public Fish getFish2() {
        return fish2;
    }

    public void setFish2(Fish fish2) {
        this.fish2 = fish2;
    }

    public float getLevel() {
        return level;
    }

    public void setLevel(float level) {
        this.level = level;
    }
}
