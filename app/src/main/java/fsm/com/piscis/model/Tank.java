package fsm.com.piscis.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Tank implements Parcelable {
    private String id;

    private String name;

    private String species;

    private int mortality;

    private String description;

    private List<Fish> fishes;

    public Tank() {
    }

    protected Tank(Parcel in) {
        id = in.readString();
        name = in.readString();
        species = in.readString();
        mortality = in.readInt();
        description = in.readString();
        fishes = in.createTypedArrayList(Fish.CREATOR);
    }

    public static final Creator<Tank> CREATOR = new Creator<Tank>() {
        @Override
        public Tank createFromParcel(Parcel in) {
            return new Tank(in);
        }

        @Override
        public Tank[] newArray(int size) {
            return new Tank[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMortality() {
        return mortality;
    }

    public void setMortality(int mortality) {
        this.mortality = mortality;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Fish> getFishes() {
        return fishes;
    }

    public void setFishes(List<Fish> fishes) {
        this.fishes = fishes;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(species);
        parcel.writeInt(mortality);
        parcel.writeString(description);
        parcel.writeTypedList(fishes);
    }
}
