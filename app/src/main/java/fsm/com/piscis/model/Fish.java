package fsm.com.piscis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Fish implements Parcelable {

    private String id;

    private String tag;

    private int size;

    private float weight;

    private int gender;

    private boolean discarded = false;

    private String locality;

    private Date dateCapture;

    private List<Date> reprodutiveHistory = new ArrayList<>();

    private String species;

    private String labId;

    private String observation;

    private Tank tank;

    @Expose(serialize = false)
    private boolean isSelected;

    public Fish() {
    }

    protected Fish(Parcel in) {
        id = in.readString();
        tag = in.readString();
        size = in.readInt();
        weight = in.readFloat();
        gender = in.readInt();
        discarded = in.readByte() != 0;
        locality = in.readString();
        species = in.readString();
        labId = in.readString();
        observation = in.readString();
        tank = in.readParcelable(Tank.class.getClassLoader());
        isSelected = in.readByte() != 0;
    }

    public static final Creator<Fish> CREATOR = new Creator<Fish>() {
        @Override
        public Fish createFromParcel(Parcel in) {
            return new Fish(in);
        }

        @Override
        public Fish[] newArray(int size) {
            return new Fish[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public boolean isDiscarded() {
        return discarded;
    }

    public void setDiscarded(boolean discarded) {
        this.discarded = discarded;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public Date getDateCapture() {
        return dateCapture;
    }

    public void setDateCapture(Date dateCapture) {
        this.dateCapture = dateCapture;
    }

    public List<Date> getReprodutiveHistory() {
        return reprodutiveHistory;
    }

    public void setReprodutiveHistory(List<Date> reprodutiveHistory) {
        this.reprodutiveHistory = reprodutiveHistory;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Tank getTank() {
        return tank;
    }

    public void setTank(Tank tank) {
        this.tank = tank;
    }

    public String getTitle() {
        String title = tag;
        if (species != null && !species.isEmpty()) {
            title = String.format("%s - %s", species, title);
        }
        if (labId != null && !labId.isEmpty()){
            title = String.format("%s [%s]", title, labId);
        }
        return title;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(tag);
        parcel.writeInt(size);
        parcel.writeFloat(weight);
        parcel.writeInt(gender);
        parcel.writeByte((byte) (discarded ? 1 : 0));
        parcel.writeString(locality);
        parcel.writeString(species);
        parcel.writeString(labId);
        parcel.writeString(observation);
        parcel.writeParcelable(tank, i);
        parcel.writeByte((byte) (isSelected ? 1 : 0));
    }

    public String getFormatedDateCapture(){
        if (dateCapture != null) {
            return SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT, Locale.forLanguageTag("pt-BR")).format(dateCapture);
        } else {
            return "";
        }
    }

    public String getFormatedWeight(){
        return String.format(Locale.forLanguageTag("pt-BR"), "%.3f Kg", weight);
    }

    public String getFormatedSize(){
        return String.format(Locale.forLanguageTag("pt-BR"), "%d cm", size);
    }
}
