package fsm.com.piscis.tank.creation;

import fsm.com.piscis.base.BasePresenter;
import fsm.com.piscis.domain.APIService;
import fsm.com.piscis.domain.Utils;
import fsm.com.piscis.domain.contracts.TankCreationContract;
import fsm.com.piscis.model.Tank;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class TankCreationPresenter extends BasePresenter<TankCreationContract.View> implements TankCreationContract.Presenter {
    public TankCreationPresenter(APIService apiService) {
        super(apiService);
    }

    @Override
    public void createTank(Tank tank) {
        if (tank == null) {
            ifViewAttached(view -> view.showError("Ocorreu um erro ao criar o tanque, tente novamente."));
        } else if (tank.getName() == null || tank.getName().isEmpty()) {
            ifViewAttached(view -> view.showError("O tanque deve possuir ao menos o nome."));
        } else {
            addSubscription(apiService
                    .createTank(tank)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tankResult -> ifViewAttached(view -> view.tankCreated(tank)),
                            throwable -> {
                                if (throwable instanceof HttpException) {
                                    if (((HttpException) throwable).code() == 401) {
                                        ifViewAttached(view -> view.logout());
                                        ;
                                    } else if (((HttpException) throwable).code() >= 500) {
                                        ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                                    } else {
                                        String message = Utils.getErrorMessage(throwable);
                                        ifViewAttached(view -> view.showError(message));
                                    }
                                } else {
                                    ifViewAttached(view -> view.showError("Ocorreu um erro inesperado."));
                                }
                            })
            );
        }
    }
}
