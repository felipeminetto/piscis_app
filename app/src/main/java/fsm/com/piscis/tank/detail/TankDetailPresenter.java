package fsm.com.piscis.tank.detail;

import fsm.com.piscis.base.BasePresenter;
import fsm.com.piscis.domain.APIService;
import fsm.com.piscis.domain.Utils;
import fsm.com.piscis.domain.contracts.TankDetailContract;
import fsm.com.piscis.model.Tank;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class TankDetailPresenter extends BasePresenter<TankDetailContract.View> implements TankDetailContract.Presenter {

    public TankDetailPresenter(APIService apiService) {
        super(apiService);
    }

    @Override
    public void loadFishesFromTank(String tankId) {
        ifViewAttached(TankDetailContract.View::showProgress);
        addSubscription(apiService
                .getFishesFromTank(tankId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(fishList -> {
                    ifViewAttached(TankDetailContract.View::hideProgress);
                    ifViewAttached(view -> view.showFishes(fishList));
                }, throwable -> {
                    ifViewAttached(TankDetailContract.View::hideProgress);
                    if (throwable instanceof HttpException) {
                        if (((HttpException) throwable).code() == 401) {
                            ifViewAttached(view -> view.logout());
                            ;
                        } else if (((HttpException) throwable).code() >= 500) {
                            ifViewAttached(TankDetailContract.View::showServerError);
                        } else {
                            String message = Utils.getErrorMessage(throwable);
                            ifViewAttached(view -> view.showError(message));
                        }
                    }
                }));
    }

    @Override
    public void deleteTank(Tank tank) {
        ifViewAttached(TankDetailContract.View::showProgress);
        addSubscription(apiService
                .removeTank(tank.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (response.isSuccessful()) {
                        ifViewAttached(TankDetailContract.View::deletedTank);
                    } else {
                        ifViewAttached(TankDetailContract.View::deleteTankError);
                    }
                }, throwable -> {
                    ifViewAttached(TankDetailContract.View::hideProgress);
                    if (throwable instanceof HttpException) {
                        if (((HttpException) throwable).code() == 401) {
                            ifViewAttached(view -> view.logout());
                            ;
                        } else if (((HttpException) throwable).code() >= 500) {
                            ifViewAttached(TankDetailContract.View::showServerError);
                        } else {
                            String message = Utils.getErrorMessage(throwable);
                            ifViewAttached(view -> view.showError(message));
                        }
                    }
                }));

    }
}
