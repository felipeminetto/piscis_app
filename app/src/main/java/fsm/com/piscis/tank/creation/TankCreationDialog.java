package fsm.com.piscis.tank.creation;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import fsm.com.piscis.PiscisApplication;
import fsm.com.piscis.R;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.databinding.AddTankFragmentBinding;
import fsm.com.piscis.domain.contracts.TankCreationContract;
import fsm.com.piscis.model.Tank;
import fsm.com.piscis.login.LoginActivity;

import static android.content.Context.MODE_PRIVATE;

public class TankCreationDialog extends DialogFragment implements TankCreationContract.View {

    private AddTankFragmentBinding binding;

    private TankCreationPresenter presenter;

    public TankCreationDialog() {
    }

    public static TankCreationDialog newInstance(String title) {
        TankCreationDialog frag = new TankCreationDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.add_tank_fragment, null, false);
        binding.setDialog(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        presenter = new TankCreationPresenter(((PiscisApplication) getContext().getApplicationContext()).getApi());
        presenter.attachView(this);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        setCancelable(false);

        String title = getArguments().getString("title", "Enter Name");
        getDialog().setTitle(title);
    }

    @Override
    public void tankCreated(Tank tank) {
        dismiss();
    }

    @Override
    public void showServerError() {
        new AlertDialog.Builder(getContext())
                .setPositiveButton(android.R.string.ok, null)
                .setMessage("Houve algum erro na comunicação com o servidor, tente novamente.")
                .setTitle("Erro")
                .create().show();
    }

    @Override
    public void showError(String message) {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .create().show();
    }

    public void dismiss() {
        getDialog().dismiss();
    }

    public void createTank() {
        Tank tank = new Tank();
        tank.setName(binding.inputName.getEditText().getText().toString());
        tank.setDescription(binding.inputDescription.getEditText().getText().toString());
        tank.setMortality(0);

        presenter.createTank(tank);
    }

    @Override
    public void logout() {
        new android.support.v7.app.AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage("A sua secção expirou! Favor efetuar o login novamente.")
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                    SharedPreferences prefs = getContext().getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
                    prefs.edit().clear().apply();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                    getActivity().finish();
                })
                .create().show();
    }
}