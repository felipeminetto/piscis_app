package fsm.com.piscis.tank.detail;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fsm.com.piscis.PiscisApplication;
import fsm.com.piscis.R;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.databinding.TankDetailFragmentBinding;
import fsm.com.piscis.domain.Utils;
import fsm.com.piscis.domain.contracts.NavigationListener;
import fsm.com.piscis.domain.contracts.SelectionListener;
import fsm.com.piscis.domain.contracts.TankDetailContract;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.model.Tank;
import fsm.com.piscis.fish.detail.FishDetailFragment;
import fsm.com.piscis.fish.list.adapter.FishAdapter;
import fsm.com.piscis.login.LoginActivity;
import fsm.com.piscis.tank.addFish.AddFishToTankFragment;

import static android.content.Context.MODE_PRIVATE;

public class TankDetailFragment extends Fragment implements TankDetailContract.View, SelectionListener {

    TankDetailFragmentBinding binding;
    private Context context;
    private NavigationListener listener;
    private TankDetailPresenter presenter;
    private Tank tank;

    public static TankDetailFragment newInstance(Tank tank) {
        TankDetailFragment fragment = new TankDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable("tank", tank);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.tank_detail_fragment, container, false);
        tank = getArguments().getParcelable("tank");
        binding.setTank(tank);
        context = getContext();
        presenter = new TankDetailPresenter(((PiscisApplication) getActivity().getApplication()).getApi());
        presenter.attachView(this);
        presenter.loadFishesFromTank(tank.getId());
        binding.rvFishes.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.txvDetail.setText(tank.getDescription());
        binding.txvInfo.setText(getString(R.string.moratility, tank.getMortality()));
        binding.txvTitle.setText(tank.getName());
        binding.fabAddFish.setOnClickListener(view -> listener.addFragmentToNavigation(AddFishToTankFragment.newInstance(tank), Constants.ADD_FISH_TO_TANK));
        if (Utils.getUser(getContext()).isAdmin()) {
            binding.btnDelete.setVisibility(View.VISIBLE);
            binding.btnDelete.setOnClickListener(view -> showDeleteTankMessage());
        } else {
            binding.btnDelete.setVisibility(View.GONE);
        }
        return binding.getRoot();
    }

    private void showDeleteTankMessage() {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage("Ao excluir este tanque, todos os peixes serão desvinculados dele.\nTem certeza que deseja excluir este tanque?")
                .setPositiveButton("Sim", (dialogInterface, i) -> {
                    presenter.deleteTank(tank);
                })
                .setNegativeButton("Não", null)
                .create().show();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Tanque");
        presenter.loadFishesFromTank(tank.getId());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showProgress() {
        binding.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.progress.setVisibility(View.GONE);
    }

    @Override
    public void showFishes(List<Fish> fishes) {
        if (fishes.isEmpty()) {
            binding.txvNoFish.setVisibility(View.VISIBLE);
            binding.rvFishes.setVisibility(View.INVISIBLE);
        } else {
            binding.txvNoFish.setVisibility(View.GONE);
            binding.rvFishes.setAdapter(new FishAdapter(fishes, this));
        }
    }

    @Override
    public void showServerError() {
        new AlertDialog.Builder(context)
                .setPositiveButton(android.R.string.ok, null)
                .setMessage("Houve algum erro na comunicação com o servidor, tente novamente.")
                .setTitle("Erro")
                .create().show();
    }

    @Override
    public void showError(String message) {
        new AlertDialog.Builder(context)
                .setPositiveButton(android.R.string.ok, null)
                .setMessage(message)
                .setTitle("Erro")
                .create().show();
    }

    @Override
    public void deletedTank() {
        new AlertDialog.Builder(context)
                .setPositiveButton(android.R.string.ok, ((dialogInterface, i) -> listener.finishFragment()))
                .setMessage("Tanque removido com sucesso!")
                .setTitle("Sucesso!")
                .create().show();
    }

    @Override
    public void deleteTankError() {
        showError("Não foi possível remover este tanque.");
    }

    public void setListener(NavigationListener listener) {
        this.listener = listener;
    }

    @Override
    public void selectedFish(Fish fish) {
        fish.setTank(tank);
        FishDetailFragment fragment = FishDetailFragment.newInstance(fish);
        fragment.setListener(listener);
        listener.addFragmentToNavigation(fragment, Constants.FISH_DETAIL);
    }

    @Override
    public void logout() {
        new android.support.v7.app.AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage("A sua secção expirou! Favor efetuar o login novamente.")
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                    SharedPreferences prefs = getContext().getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
                    prefs.edit().clear().apply();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                    getActivity().finish();
                })
                .create().show();
    }
}
