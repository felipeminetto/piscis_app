package fsm.com.piscis.tank.addFish.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import fsm.com.piscis.R;
import fsm.com.piscis.databinding.SelectFishItemBinding;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.domain.contracts.SelectionListener;

public class SelectFishAdapter extends RecyclerView.Adapter<SelectFishViewHolder> {

    private List<Fish> fishes;
    private SelectionListener selectionListener;

    public SelectFishAdapter(List<Fish> fishes, SelectionListener selectionListener) {
        this.fishes = fishes;
        this.selectionListener = selectionListener;
    }

    @NonNull
    @Override
    public SelectFishViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SelectFishItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.select_fish_item, parent, false);
        return new SelectFishViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectFishViewHolder holder, int position) {
        holder.bind(fishes.get(position), selectionListener);
    }

    @Override
    public int getItemCount() {
        return fishes.size();
    }
}
