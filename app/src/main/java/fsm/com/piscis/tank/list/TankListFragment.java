package fsm.com.piscis.tank.list;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.clans.fab.FloatingActionButton;

import java.util.List;

import fsm.com.piscis.PiscisApplication;
import fsm.com.piscis.R;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.databinding.ListFragmentBinding;
import fsm.com.piscis.domain.Utils;
import fsm.com.piscis.domain.contracts.NavigationListener;
import fsm.com.piscis.domain.contracts.TankContract;
import fsm.com.piscis.domain.contracts.TankSelectionListener;
import fsm.com.piscis.model.Tank;
import fsm.com.piscis.model.User;
import fsm.com.piscis.login.LoginActivity;
import fsm.com.piscis.tank.creation.TankCreationDialog;
import fsm.com.piscis.tank.detail.TankDetailFragment;
import fsm.com.piscis.tank.list.adapter.TankAdapter;

import static android.content.Context.MODE_PRIVATE;

public class TankListFragment extends Fragment implements TankContract.View, TankSelectionListener {

    private ListFragmentBinding binding;

    private TankPresenter presenter;

    private NavigationListener listener;

    private Context context;

    public static TankListFragment newInstance() {
        return new TankListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false);
        if (getContext() != null) {
            this.context = getContext();
        }
        presenter = new TankPresenter(((PiscisApplication) getContext().getApplicationContext()).getApi());
        presenter.attachView(this);
        binding.swipeRefreshLayout.setOnRefreshListener(presenter::loadTanks);
        presenter.loadTanks();
        User user = Utils.getUser(getContext());
        if (user.isAdmin()) {
            setupFloatingMenu();
        }
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Tanques");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showTanks(List<Tank> tanks) {
        binding.rvItems.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvItems.setAdapter(new TankAdapter(tanks, this));
    }

    @Override
    public void showProgress() {
        binding.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.swipeRefreshLayout.setRefreshing(false);
        binding.progress.setVisibility(View.GONE);
    }

    @Override
    public void showServerError() {
        showError("Houve algum erro na comunicação com o servidor, tente novamente.");
    }

    @Override
    public void showError(String message) {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .create().show();
    }

    private void setupFloatingMenu() {
        FloatingActionButton fabCreate = new FloatingActionButton(context);
        fabCreate.setButtonSize(FloatingActionButton.SIZE_MINI);
        fabCreate.setLabelText("Novo Tanque");
        Drawable d = getResources().getDrawable(R.drawable.add);
        fabCreate.setColorNormal(getResources().getColor(R.color.colorAccent));
        fabCreate.setImageDrawable(d);
        fabCreate.setOnClickListener(view -> openCreationDialog());
        binding.floatingMenu.addMenuButton(fabCreate);

        binding.floatingMenu.setVisibility(View.VISIBLE);
    }

    private void openCreationDialog() {
        TankCreationDialog dialog = TankCreationDialog.newInstance("Novo tanque");
        dialog.show(getFragmentManager(), Constants.ADD_TANK);
    }

    public void setListener(NavigationListener listener) {
        this.listener = listener;
    }

    @Override
    public void selectedTank(Tank tank) {
        TankDetailFragment fragment = TankDetailFragment.newInstance(tank);
        fragment.setListener(listener);
        listener.addFragmentToNavigation(fragment, Constants.FISH_DETAIL);
    }

    @Override
    public void logout() {
        new android.support.v7.app.AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage("A sua secção expirou! Favor efetuar o login novamente.")
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                    SharedPreferences prefs = getContext().getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
                    prefs.edit().clear().apply();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                    getActivity().finish();
                })
                .create().show();
    }
}
