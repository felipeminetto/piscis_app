package fsm.com.piscis.tank.addFish;

import android.util.Log;

import java.util.List;

import fsm.com.piscis.base.BasePresenter;
import fsm.com.piscis.domain.APIService;
import fsm.com.piscis.domain.Utils;
import fsm.com.piscis.domain.contracts.AddFishToTankContract;
import fsm.com.piscis.domain.contracts.AddFishToTankContract.Presenter;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.model.Tank;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class AddFishToTankPresenter extends BasePresenter<AddFishToTankContract.View> implements Presenter {

    public AddFishToTankPresenter(APIService apiService) {
        super(apiService);
    }

    @Override
    public void addFishesToTank(List<Fish> fishes, Tank tank) {
        tank.setFishes(fishes);
        addSubscription(apiService
                .addFishesToTank(tank)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tankResponse -> ifViewAttached(AddFishToTankContract.View::fishesAdded), throwable -> {
                    Log.e("adding fishes to tank", throwable.getMessage());
                    if (throwable instanceof HttpException) {
                        if (((HttpException) throwable).code() == 401) {
                            ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                        } else if (((HttpException) throwable).code() >= 500) {
                            ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                        } else {
                            String message = Utils.getErrorMessage(throwable);
                            ifViewAttached(view -> view.showError(message));
                        }
                    } else {
                        ifViewAttached(view -> view.showError("Ocorreu um erro inesperado."));
                    }
                }));
    }

    @Override
    public void loadFishesOutTank(Tank tank) {
        ifViewAttached(AddFishToTankContract.View::showProgress);
        addSubscription(apiService
                .getFishesOutTank(tank.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(fishes -> {
                            ifViewAttached(AddFishToTankContract.View::hideProgress);
                            ifViewAttached(view -> view.showFishes(fishes));
                        },
                        throwable -> {
                            ifViewAttached(AddFishToTankContract.View::hideProgress);
                            if (throwable instanceof HttpException) {
                                if (((HttpException) throwable).code() == 401) {
                                    ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                                } else if (((HttpException) throwable).code() >= 500) {
                                    ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                                } else {
                                    String message = Utils.getErrorMessage(throwable);
                                    ifViewAttached(view -> view.showError(message));
                                }
                            } else {
                                ifViewAttached(view -> view.showError("Ocorreu um erro inesperado."));
                            }
                            Log.e("get fishes out tank", throwable.getMessage());
                        }));
    }
}
