package fsm.com.piscis.tank.addFish;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import fsm.com.piscis.PiscisApplication;
import fsm.com.piscis.R;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.databinding.ListFragmentBinding;
import fsm.com.piscis.domain.contracts.AddFishToTankContract;
import fsm.com.piscis.domain.contracts.SelectionListener;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.model.Tank;
import fsm.com.piscis.login.LoginActivity;
import fsm.com.piscis.tank.addFish.adapter.SelectFishAdapter;

import static android.content.Context.MODE_PRIVATE;

public class AddFishToTankFragment extends Fragment implements AddFishToTankContract.View, SelectionListener {

    private ListFragmentBinding binding;

    private Tank tank;

    private AddFishToTankPresenter presenter;

    private List<Fish> selectedFishes = new ArrayList<>();

    private Context context;

    public static AddFishToTankFragment newInstance(Tank tank) {
        AddFishToTankFragment fragment = new AddFishToTankFragment();
        Bundle args = new Bundle();
        args.putParcelable("tank", tank);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false);
        context = getContext();
        tank = getArguments().getParcelable("tank");
        binding.floatingMenu.setVisibility(View.GONE);
        binding.rvItems.setLayoutManager(new LinearLayoutManager(getContext()));
        presenter = new AddFishToTankPresenter(((PiscisApplication) getActivity().getApplication()).getApi());
        presenter.attachView(this);
        presenter.loadFishesOutTank(tank);
        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            presenter.loadFishesOutTank(tank);
            binding.swipeRefreshLayout.setRefreshing(false);
        });
        setHasOptionsMenu(true);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Selecione os peixes");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.selection_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_add) {
            presenter.addFishesToTank(selectedFishes, tank);
            return true;
        }
        return false;
    }

    @Override
    public void showServerError() {
        new AlertDialog.Builder(context)
                .setPositiveButton(android.R.string.ok, null)
                .setMessage("Houve algum erro na comunicação com o servidor, tente novamente.")
                .setTitle("Erro")
                .create().show();
    }

    @Override
    public void fishesAdded() {
        getFragmentManager().popBackStack();
    }

    @Override
    public void showError(String message) {
        new AlertDialog.Builder(context)
                .setPositiveButton(android.R.string.ok, null)
                .setMessage(message)
                .setTitle("Atenção")
                .create().show();
    }

    @Override
    public void showProgress() {
        binding.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.progress.setVisibility(View.GONE);
    }

    @Override
    public void showFishes(List<Fish> fishes) {
        SelectFishAdapter adapter = new SelectFishAdapter(fishes, this);
        binding.rvItems.setAdapter(adapter);
    }

    @Override
    public void selectedFish(Fish fish) {
        if (fish.isSelected()) {
            if (!selectedFishes.contains(fish))
                selectedFishes.add(fish);
        } else {
            selectedFishes.remove(fish);
        }
    }

    @Override
    public void logout() {
        new android.support.v7.app.AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage("A sua secção expirou! Favor efetuar o login novamente.")
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                    SharedPreferences prefs = getContext().getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
                    prefs.edit().clear().apply();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                    getActivity().finish();
                })
                .create().show();
    }
}
