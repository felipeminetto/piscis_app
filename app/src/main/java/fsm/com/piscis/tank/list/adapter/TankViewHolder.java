package fsm.com.piscis.tank.list.adapter;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;

import java.text.MessageFormat;

import fsm.com.piscis.R;
import fsm.com.piscis.databinding.TankItemBinding;
import fsm.com.piscis.domain.contracts.TankSelectionListener;
import fsm.com.piscis.model.Tank;

public class TankViewHolder extends RecyclerView.ViewHolder {

    private TankItemBinding binding;

    public TankViewHolder(TankItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(Tank tank, TankSelectionListener listener) {
        binding.setTank(tank);
        Resources resources = binding.getRoot().getContext().getResources();
        String fmt = resources.getText(R.string.fish_count).toString();
        String fishCounter = MessageFormat.format(fmt, tank.getFishes().size());
        if (tank.getSpecies() != null && !tank.getSpecies().isEmpty()) {
            binding.txtFishCount.setText(String.format("%s - %s", fishCounter, tank.getSpecies()));
        } else {
            binding.txtFishCount.setText(fishCounter);
        }
        binding.getRoot().setOnClickListener(view -> listener.selectedTank(tank));
        binding.executePendingBindings();
    }
}
