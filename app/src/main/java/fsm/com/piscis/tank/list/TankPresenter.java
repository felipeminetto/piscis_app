package fsm.com.piscis.tank.list;

import android.util.Log;

import fsm.com.piscis.base.BasePresenter;
import fsm.com.piscis.domain.APIService;
import fsm.com.piscis.domain.Utils;
import fsm.com.piscis.domain.contracts.TankContract;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

import static android.content.ContentValues.TAG;

public class TankPresenter extends BasePresenter<TankContract.View> implements TankContract.Presenter {
    public TankPresenter(APIService apiService) {
        super(apiService);
    }

    @Override
    public void loadTanks() {
        ifViewAttached(TankContract.View::showProgress);
        addSubscription(apiService.getAllTanks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tankList -> {
                            ifViewAttached(view -> view.showTanks(tankList));
                            ifViewAttached(TankContract.View::hideProgress);
                        },
                        throwable -> {
                            ifViewAttached(TankContract.View::hideProgress);
                            if (throwable instanceof HttpException) {
                                if (((HttpException) throwable).code() == 401) {
                                    ifViewAttached(view -> view.logout());
                                    ;
                                } else if (((HttpException) throwable).code() >= 500) {
                                    ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                                } else {
                                    String message = Utils.getErrorMessage(throwable);
                                    ifViewAttached(view -> view.showError(message));
                                }
                            }
                            Log.e(TAG, "loadFishes: ", throwable);
                        })
        );
    }
}
