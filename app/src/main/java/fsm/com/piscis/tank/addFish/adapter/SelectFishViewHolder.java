package fsm.com.piscis.tank.addFish.adapter;

import android.support.v7.widget.RecyclerView;

import fsm.com.piscis.databinding.SelectFishItemBinding;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.domain.contracts.SelectionListener;

public class SelectFishViewHolder extends RecyclerView.ViewHolder {

    private SelectFishItemBinding binding;

    public SelectFishViewHolder(SelectFishItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(Fish fish, SelectionListener selectionListener) {
        binding.setFish(fish);
        binding.ckbFish.setOnCheckedChangeListener((compoundButton, checked) -> {
            fish.setSelected(checked);
        });
        binding.getRoot().setOnClickListener(view -> {
            binding.ckbFish.setChecked(!fish.isSelected());
            selectionListener.selectedFish(fish);
        });

        binding.executePendingBindings();
    }
}
