package fsm.com.piscis.tank.list.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import fsm.com.piscis.R;
import fsm.com.piscis.databinding.TankItemBinding;
import fsm.com.piscis.domain.contracts.TankSelectionListener;
import fsm.com.piscis.model.Tank;

public class TankAdapter extends RecyclerView.Adapter<TankViewHolder> {

    private List<Tank> tanks;
    private TankSelectionListener listener;

    public TankAdapter(List<Tank> tanks, TankSelectionListener listener) {
        this.tanks = tanks;
        this.listener = listener;
    }

    @NonNull
    @Override
    public TankViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TankItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.tank_item, parent, false);
        return new TankViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TankViewHolder holder, int position) {
        holder.bind(tanks.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return tanks.size();
    }
}
