package fsm.com.piscis.fish.list;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.clans.fab.FloatingActionButton;

import java.util.List;

import fsm.com.piscis.PiscisApplication;
import fsm.com.piscis.R;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.databinding.FindFishDialogBinding;
import fsm.com.piscis.databinding.ListFragmentBinding;
import fsm.com.piscis.domain.contracts.FishContract;
import fsm.com.piscis.domain.contracts.NavigationListener;
import fsm.com.piscis.domain.contracts.SelectionListener;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.fish.creation.FishCreationDialog;
import fsm.com.piscis.fish.detail.FishDetailFragment;
import fsm.com.piscis.fish.list.adapter.FishAdapter;
import fsm.com.piscis.login.LoginActivity;

import static android.content.Context.MODE_PRIVATE;

public class FishesListFragment extends Fragment implements FishContract.View, SelectionListener {

    private ListFragmentBinding binding;

    private FishPresenter presenter;

    private NavigationListener listener;

    private Context context;

    public static FishesListFragment newInstance() {
        return new FishesListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false);
        if (getContext() != null) {
            this.context = getContext();
        }
        presenter = new FishPresenter(((PiscisApplication) context.getApplicationContext()).getApi());
        presenter.attachView(this);
        setupFloatingMenu();
        binding.swipeRefreshLayout.setOnRefreshListener(presenter::loadFishes);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Peixes");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    private void setupFloatingMenu() {
        FloatingActionButton fabCreate = new FloatingActionButton(context);
        fabCreate.setButtonSize(FloatingActionButton.SIZE_MINI);
        fabCreate.setLabelText("Novo peixe");
        fabCreate.setColorNormal(getResources().getColor(R.color.colorAccent));
        fabCreate.setImageDrawable(getResources().getDrawable(R.drawable.add));
        fabCreate.setOnClickListener(view -> openCreationDialog());
        binding.floatingMenu.addMenuButton(fabCreate);

        FloatingActionButton fabTag = new FloatingActionButton(context);
        fabTag.setButtonSize(FloatingActionButton.SIZE_MINI);
        fabTag.setColorNormal
                (getResources().getColor(R.color.colorAccent));
        fabTag.setLabelText("Encontrar peixe");
        fabTag.setImageDrawable(getResources().getDrawable(R.drawable.rfid));
        fabTag.setOnClickListener(view -> openTagDialog());
        binding.floatingMenu.addMenuButton(fabTag);

        binding.floatingMenu.setVisibility(View.VISIBLE);
    }

    private void openTagDialog() {
        FindFishDialogBinding findFishBinding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.find_fish_dialog,
                null, false);
        new AlertDialog.Builder(getContext())
                .setTitle("Encontrar peixe")
                .setView(findFishBinding.getRoot())
                .setPositiveButton("Encontrar", (dialogInterface, i) -> {
                    presenter.findFish(findFishBinding.edtId.getText().toString());
                    dialogInterface.dismiss();
                })
                .setNegativeButton("Cancelar", null)
                .create().show();
    }

    private void openCreationDialog() {
        FishCreationDialog dialog = FishCreationDialog.newInstance("Novo Peixe");
        dialog.show(getFragmentManager(), Constants.ADD_FISH);
        binding.floatingMenu.close(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.loadFishes();
    }

    @Override
    public void showFishes(List<Fish> fishes) {
        binding.rvItems.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvItems.setAdapter(new FishAdapter(fishes, this));
        binding.executePendingBindings();
    }

    @Override
    public void showFishDetail(Fish fish) {
        selectedFish(fish);
    }

    @Override
    public void showProgress() {
        binding.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.swipeRefreshLayout.setRefreshing(false);
        binding.progress.setVisibility(View.GONE);
    }

    public void setListener(NavigationListener listener) {
        this.listener = listener;
    }

    @Override
    public void selectedFish(Fish fish) {
        FishDetailFragment fragment = FishDetailFragment.newInstance(fish);
        fragment.setListener(listener);
        listener.addFragmentToNavigation(fragment, Constants.FISH_DETAIL);
    }

    @Override
    public void showError(String message) {
        new AlertDialog.Builder(getContext())
                .setPositiveButton(android.R.string.ok, null)
                .setMessage(message)
                .setTitle("Erro")
                .create().show();
    }

    @Override
    public void showServerError() {
        new AlertDialog.Builder(getContext())
                .setPositiveButton(android.R.string.ok, null)
                .setMessage("Houve algum erro na comunicação com o servidor, tente novamente.")
                .setTitle("Erro")
                .create().show();
    }

    @Override
    public void logout() {
        new android.support.v7.app.AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage("A sua secção expirou! Favor efetuar o login novamente.")
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                    SharedPreferences prefs = getContext().getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
                    prefs.edit().clear().apply();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                    getActivity().finish();
                })
                .create().show();
    }
}
