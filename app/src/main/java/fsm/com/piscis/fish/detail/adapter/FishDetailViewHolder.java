package fsm.com.piscis.fish.detail.adapter;

import android.support.v7.widget.RecyclerView;

import fsm.com.piscis.R;
import fsm.com.piscis.databinding.FishCrossingItemBinding;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.model.KinshipLevel;
import fsm.com.piscis.domain.contracts.SelectionListener;

public class FishDetailViewHolder extends RecyclerView.ViewHolder {

    private FishCrossingItemBinding binding;

    public FishDetailViewHolder(FishCrossingItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(KinshipLevel match, Fish fish, SelectionListener selectionListener) {
        binding.setFish(match.getFish1().getId().equalsIgnoreCase(fish.getId()) ? match.getFish2() : match.getFish1());
        binding.layout.setBackgroundResource(match.getLevel() > 0.375 ? R.drawable.kl_danger_gardient
                : match.getLevel() > 0.25 ? R.drawable.kl_warning_gardient
                : R.drawable.kl_ok_gardient);
        binding.getRoot().setOnClickListener(view -> {
            selectionListener.selectedFish( binding.getFish());
        });

        binding.executePendingBindings();
    }
}
