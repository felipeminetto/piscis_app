package fsm.com.piscis.fish.move;

import android.util.Log;

import fsm.com.piscis.base.BasePresenter;
import fsm.com.piscis.domain.APIService;
import fsm.com.piscis.domain.Utils;
import fsm.com.piscis.domain.contracts.MoveFishToTankContract;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.model.Tank;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

import static android.content.ContentValues.TAG;

public class MoveFishToTankPresenter extends BasePresenter<MoveFishToTankContract.View> implements MoveFishToTankContract.Presenter {
    public MoveFishToTankPresenter(APIService apiService) {
        super(apiService);
    }

    @Override
    public void moveFishToTank(Fish fish, Tank tank) {
        ifViewAttached(MoveFishToTankContract.View::showProgress);
        addSubscription(apiService.addFishToTank(fish.getId(), tank.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tank1 -> {
                    ifViewAttached(MoveFishToTankContract.View::hideProgress);
                    fish.setTank(tank);
                    ifViewAttached(view -> view.fishAddedToTank(fish));
                }, throwable -> {
                    ifViewAttached(MoveFishToTankContract.View::hideProgress);
                    if (throwable instanceof HttpException) {
                        if (((HttpException) throwable).code() == 401) {
                            ifViewAttached(view -> view.logout());
                            ;
                        } else if (((HttpException) throwable).code() >= 500) {
                            ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                        } else {
                            String message = Utils.getErrorMessage(throwable);
                            ifViewAttached(view -> view.showError(message));
                        }
                    }
                    Log.e(TAG, "loadFishes: ", throwable);
                }));
    }

    @Override
    public void loadTanks() {
        ifViewAttached(MoveFishToTankContract.View::showProgress);
        addSubscription(apiService.getAllTanks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tankList -> {
                            ifViewAttached(view -> view.showTanks(tankList));
                            ifViewAttached(MoveFishToTankContract.View::hideProgress);
                        },
                        throwable -> {
                            ifViewAttached(MoveFishToTankContract.View::hideProgress);
                            if (throwable instanceof HttpException) {
                                if (((HttpException) throwable).code() == 401) {
                                    ifViewAttached(view -> view.logout());
                                } else if (((HttpException) throwable).code() >= 500) {
                                    ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                                } else {
                                    String message = Utils.getErrorMessage(throwable);
                                    ifViewAttached(view -> view.showError(message));
                                }
                            }
                            Log.e(TAG, "loadFishes: ", throwable);
                        })
        );
    }
}
