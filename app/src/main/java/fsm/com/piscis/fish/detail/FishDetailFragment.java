package fsm.com.piscis.fish.detail;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.github.clans.fab.FloatingActionButton;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import fsm.com.piscis.PiscisApplication;
import fsm.com.piscis.R;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.databinding.FishDetailFragmentBinding;
import fsm.com.piscis.domain.contracts.FishDetailContract;
import fsm.com.piscis.domain.contracts.NavigationListener;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.fish.dir_reprod.ReprodutiveDirectionFragment;
import fsm.com.piscis.fish.edit.FishEditFragment;
import fsm.com.piscis.fish.move.MoveFishToTankFragment;
import fsm.com.piscis.login.LoginActivity;
import fsm.com.piscis.tank.detail.TankDetailFragment;
import fsm.com.piscis.utils.MyDatePicker;

import static android.content.Context.MODE_PRIVATE;

public class FishDetailFragment extends Fragment implements MyDatePicker.DateSelectedListener, FishDetailContract.View {

    FishDetailFragmentBinding binding;
    private Fish fish;
    private NavigationListener listener;
    private FishDetailPresenter presenter;

    public static FishDetailFragment newInstance(Fish fish) {
        FishDetailFragment fragment = new FishDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("fish", fish);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fish_detail_fragment, container, false);
        fish = getArguments().getParcelable("fish");
        binding.setFragment(this);
        binding.setFish(fish);
        String fmt = getResources().getText(R.string.reproductions).toString();
        binding.txvReprodCount.setText(MessageFormat.format(fmt, fish.getReprodutiveHistory().size()));
        presenter = new FishDetailPresenter(((PiscisApplication) getActivity().getApplication()).getApi());
        presenter.attachView(this);
        setupFloatingMenu();
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Peixe");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    private void setupFloatingMenu() {
        if (!fish.isDiscarded()) {
            FloatingActionButton fabAddReprodDate = new FloatingActionButton(getContext());
            fabAddReprodDate.setButtonSize(FloatingActionButton.SIZE_MINI);
            fabAddReprodDate.setLabelText("Adicionar data de reprodução");
            fabAddReprodDate.setColorNormal(getResources().getColor(R.color.colorAccent));
            Drawable dCalendar = getResources().getDrawable(R.drawable.calendar);
            dCalendar.setTint(getResources().getColor(R.color.white));
            fabAddReprodDate.setImageDrawable(dCalendar);
            fabAddReprodDate.setOnClickListener(view -> addReprodDate());
            binding.floatingMenu.addMenuButton(fabAddReprodDate);

            FloatingActionButton fabTag = new FloatingActionButton(getContext());
            fabTag.setButtonSize(FloatingActionButton.SIZE_MINI);
            fabTag.setColorNormal(getResources().getColor(R.color.colorAccent));
            fabTag.setLabelText("Editar peixe");
            Drawable dEdit = getResources().getDrawable(R.drawable.ic_edit);
            dEdit.setTint(getResources().getColor(R.color.white));
            fabTag.setImageDrawable(dEdit);
            fabTag.setOnClickListener(view -> editFish(fish));
            binding.floatingMenu.addMenuButton(fabTag);

            FloatingActionButton fabDiscard = new FloatingActionButton(getContext());
            fabDiscard.setButtonSize(FloatingActionButton.SIZE_MINI);
            fabDiscard.setColorNormal(getResources().getColor(R.color.colorAccent));
            fabDiscard.setLabelText("Descartar peixe");
            Drawable dDiscard = getResources().getDrawable(R.drawable.delete);
            dDiscard.setTint(getResources().getColor(R.color.white));
            fabDiscard.setImageDrawable(dDiscard);
            fabDiscard.setOnClickListener(view -> discardFish(fish));
            binding.floatingMenu.addMenuButton(fabDiscard);
            binding.floatingMenu.setVisibility(View.VISIBLE);

            FloatingActionButton fabMoveToTank = new FloatingActionButton(getContext());
            fabMoveToTank.setButtonSize(FloatingActionButton.SIZE_MINI);
            fabMoveToTank.setColorNormal(getResources().getColor(R.color.colorAccent));
            fabMoveToTank.setLabelText("Mover para tanque");
            Drawable dTank = getResources().getDrawable(R.drawable.water);
            dTank.setTint(getResources().getColor(R.color.white));
            fabMoveToTank.setImageDrawable(dTank);
            fabMoveToTank.setOnClickListener(view -> openTankSelection(fish));
            binding.floatingMenu.addMenuButton(fabMoveToTank);
            binding.floatingMenu.setVisibility(View.VISIBLE);
        } else {
            binding.floatingMenu.setVisibility(View.GONE);
        }
    }

    private void discardFish(Fish fish) {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage("Tem certeza que desea descartar este peixe?")
                .setPositiveButton("Sim", (dialogInterface, i) -> {
                    presenter.discardFish(fish);
                })
                .setNegativeButton("Não", null)
                .create().show();
    }

    private void openTankSelection(Fish fish) {
        MoveFishToTankFragment fragment = MoveFishToTankFragment.newInstance(fish);
        fragment.setListener(listener);
        listener.addFragmentToNavigation(fragment, Constants.MOVE_FISH);
    }

    private void editFish(Fish fish) {
        FishEditFragment fragment = FishEditFragment.newInstance(fish);
        fragment.setListener(listener);
        listener.addFragmentToNavigation(fragment, Constants.EDIT_FISH);
    }

    private void addReprodDate() {
        showDatePicker();
    }

    private void showDatePicker() {
        MyDatePicker datePicker = new MyDatePicker();
        datePicker.setListener(this);
        datePicker.show(getFragmentManager(), "date_picker");
    }

    @Override
    public void onPickerCanceled() {
    }

    @Override
    public void dateSelected(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date d = new Date(calendar.getTimeInMillis());
        presenter.addReproductionDate(d, fish);
    }

    public void showTank() {
        TankDetailFragment fragment = TankDetailFragment.newInstance(fish.getTank());
        fragment.setListener(listener);
        listener.addFragmentToNavigation(fragment, Constants.TANK);
    }

    public void showReprodutiveDirection() {
        ReprodutiveDirectionFragment fragment = ReprodutiveDirectionFragment.newInstance(fish);
        fragment.setListener(listener);
        listener.addFragmentToNavigation(fragment, Constants.REPROD_DIRECT);
    }

    public void showReprodutiveHistory() {
        List<String> datas = new ArrayList<>();
        DateFormat dateFormat = SimpleDateFormat.getDateInstance(SimpleDateFormat.DEFAULT, Locale.forLanguageTag("pt-BR"));
        for (Date date : fish.getReprodutiveHistory()) {
            datas.add(dateFormat.format(date));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Histórico reprodutivo");
        builder.setPositiveButton(android.R.string.ok, null);
        if (datas.size() > 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, datas);
            builder.setAdapter(adapter, null);
        } else {
            builder.setMessage("Nenhuma reprodução até o momento.");
        }
        builder.create().show();
    }

    public void setListener(NavigationListener listener) {
        this.listener = listener;
    }

    @Override
    public void showProgress() {
        binding.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.progress.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .create().show();
    }

    @Override
    public void showServerError() {
        new AlertDialog.Builder(getContext())
                .setPositiveButton(android.R.string.ok, null)
                .setMessage("Houve algum erro na comunicação com o servidor, tente novamente.")
                .setTitle("Erro")
                .create().show();
    }

    @Override
    public void showSuccess(String message) {
        new AlertDialog.Builder(getContext())
                .setTitle("Sucesso!")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .create().show();
    }

    @Override
    public void updateFish(Fish fish) {
        this.fish = fish;
        String fmt = getResources().getText(R.string.reproductions).toString();
        binding.txvReprodCount.setText(MessageFormat.format(fmt, fish.getReprodutiveHistory().size()));
        binding.setFish(fish);
    }

    @Override
    public void logout() {
        new android.support.v7.app.AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage("A sua secção expirou! Favor efetuar o login novamente.")
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                    SharedPreferences prefs = getContext().getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
                    prefs.edit().clear().apply();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                    getActivity().finish();
                })
                .create().show();
    }
}
