package fsm.com.piscis.fish.dir_reprod;

import android.util.Log;

import fsm.com.piscis.base.BasePresenter;
import fsm.com.piscis.domain.APIService;
import fsm.com.piscis.domain.contracts.ReprodutiveDirectionContract;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ReprodutiveDirectionPresenter extends BasePresenter<ReprodutiveDirectionContract.View> implements ReprodutiveDirectionContract.Presenter {

    public ReprodutiveDirectionPresenter(APIService apiService) {
        super(apiService);
    }

    @Override
    public void loadMatchesForFish(String fishId) {
        ifViewAttached(ReprodutiveDirectionContract.View::showProgress);
        addSubscription(apiService
                .getKinsForFish(fishId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(fishList -> {
                    ifViewAttached(ReprodutiveDirectionContract.View::hideProgress);
                    ifViewAttached(view -> view.showMatchFishes(fishList));
                }, throwable -> {
                    ifViewAttached(ReprodutiveDirectionContract.View::hideProgress);
                    ifViewAttached(ReprodutiveDirectionContract.View::showServerError);
                    Log.e("loaad matches ", "Error loading matches for fish");
                    Log.e("loaad matches ", throwable.getMessage());
                }));
    }
}
