package fsm.com.piscis.fish.move;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fsm.com.piscis.PiscisApplication;
import fsm.com.piscis.R;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.databinding.ListFragmentBinding;
import fsm.com.piscis.domain.contracts.MoveFishToTankContract;
import fsm.com.piscis.domain.contracts.NavigationListener;
import fsm.com.piscis.domain.contracts.TankSelectionListener;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.model.Tank;
import fsm.com.piscis.login.LoginActivity;
import fsm.com.piscis.tank.list.adapter.TankAdapter;

import static android.content.Context.MODE_PRIVATE;

public class MoveFishToTankFragment extends Fragment implements MoveFishToTankContract.View, TankSelectionListener {

    private ListFragmentBinding binding;

    private MoveFishToTankPresenter presenter;

    private NavigationListener listener;

    private Fish fish;

    public static MoveFishToTankFragment newInstance(Fish fish) {
        MoveFishToTankFragment fragment = new MoveFishToTankFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("fish", fish);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false);
        this.fish = getArguments().getParcelable("fish");
        presenter = new MoveFishToTankPresenter(((PiscisApplication) getContext().getApplicationContext()).getApi());
        presenter.attachView(this);
        binding.swipeRefreshLayout.setOnRefreshListener(presenter::loadTanks);
        presenter.loadTanks();
        return binding.getRoot();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Selecione o tanque");
    }

    @Override
    public void showTanks(List<Tank> tanks) {
        binding.rvItems.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvItems.setAdapter(new TankAdapter(tanks, this));
    }

    @Override
    public void showProgress() {
        binding.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.swipeRefreshLayout.setRefreshing(false);
        binding.progress.setVisibility(View.GONE);
    }

    @Override
    public void showServerError() {
        showError("Houve algum erro na comunicação com o servidor, tente novamente.");
    }

    @Override
    public void fishAddedToTank(Fish fish) {
        listener.finishAndUpdateFish(fish);
    }

    @Override
    public void showError(String message) {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .create().show();
    }

    public void setListener(NavigationListener listener) {
        this.listener = listener;
    }

    @Override
    public void selectedTank(Tank tank) {
        presenter.moveFishToTank(fish, tank);
    }

    @Override
    public void logout() {
        new android.support.v7.app.AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage("A sua secção expirou! Favor efetuar o login novamente.")
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                    SharedPreferences prefs = getContext().getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
                    prefs.edit().clear().apply();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                    getActivity().finish();
                })
                .create().show();
    }
}
