package fsm.com.piscis.fish.creation;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import fsm.com.piscis.PiscisApplication;
import fsm.com.piscis.R;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.databinding.AddFishFragmentBinding;
import fsm.com.piscis.domain.contracts.FishCreationContract;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.login.LoginActivity;
import fsm.com.piscis.utils.MyDatePicker;

import static android.content.Context.MODE_PRIVATE;

public class FishCreationDialog extends DialogFragment implements FishCreationContract.View, MyDatePicker.DateSelectedListener {

    private AddFishFragmentBinding binding;

    private FishCreationPresnter presenter;

    private Date selectedDate;

    public FishCreationDialog() {
    }

    public static FishCreationDialog newInstance(String title) {
        FishCreationDialog frag = new FishCreationDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.add_fish_fragment, null, false);
        binding.setDialog(this);

        binding.inputSize.getEditText().setOnFocusChangeListener((view, hasFocus) -> {
            String text = binding.inputSize.getEditText().getText().toString();
            if (!hasFocus && text.isEmpty()) {
                binding.inputSize.getEditText().setText("0");
            }
        });

        binding.inputWeight.getEditText().setOnFocusChangeListener((view, hasFocus) -> {
            String text = binding.inputWeight.getEditText().getText().toString();
            if (!hasFocus && text.isEmpty()) {
                binding.inputWeight.getEditText().setText("0.0");
            }
        });

        binding.inputDateCapture.getEditText().setShowSoftInputOnFocus(false);
        binding.inputDateCapture.getEditText().setOnFocusChangeListener((view, b) -> {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            if (b) showDatePicker();
        });
        return binding.getRoot();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        presenter.detachView();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        presenter = new FishCreationPresnter(((PiscisApplication) getActivity().getApplicationContext()).getApi());
        presenter.attachView(this);
//        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        setCancelable(false);

        String title = getArguments().getString("title", "Enter Name");
        getDialog().setTitle(title);
    }

    public void saveFish() {
        Fish fish = new Fish();

        fish.setTag(binding.inputTag.getEditText().getText().toString());
        fish.setSpecies(binding.inputSpecies.getEditText().getText().toString());
        fish.setLocality(binding.inputLocality.getEditText().getText().toString());
        fish.setDateCapture(selectedDate);
        fish.setSize(Integer.parseInt(binding.inputSize.getEditText().getText().toString()));
        fish.setWeight(Float.parseFloat(binding.inputWeight.getEditText().getText().toString()));
        fish.setGender(binding.spnGender.getSelectedItemPosition());
        fish.setObservation(binding.inputObs.getEditText().getText().toString());

        presenter.saveFish(fish);
    }

    @Override
    public void showTagWarning(Fish fish) {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage("Você não adicionou uma tag a este peixe. Deseja criar o peixe mesmo assim?")
                .setPositiveButton("Sim", (dialogInterface, i) -> presenter.createFish(fish, false))
                .setNegativeButton("Não", null)
                .create().show();
    }

    @Override
    public void fishSaved() {
        dismiss();
    }

    public void showDatePicker() {
        MyDatePicker datePicker = new MyDatePicker();
        datePicker.setListener(this);
        datePicker.show(getFragmentManager(), "date_picker");
    }

    @Override
    public void dateSelected(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date d = new Date(calendar.getTimeInMillis());
        selectedDate = d;
        DateFormat dateFormat = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT, Locale.forLanguageTag("pt-BR"));
        binding.inputDateCapture.getEditText().setText(dateFormat.format(d));
        binding.inputSize.getEditText().requestFocus();
    }

    @Override
    public void showTagError(Fish fish, String message) {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage(message)
                .setPositiveButton("Descartar e cadastrar", (dialogInterface, i) -> {
                    presenter.createFish(fish, true);
                })
                .setNegativeButton("Cancelar", null)
                .create().show();

    }

    @Override
    public void showError(String message) {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .create().show();
    }

    @Override
    public void logout() {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage("A sua secção expirou! Favor efetuar o login novamente.")
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                    SharedPreferences prefs = getContext().getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
                    prefs.edit().clear().apply();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                    getActivity().finish();
                })
                .create().show();
    }

    @Override
    public void showServerError() {
        showError("Houve algum erro na comunicação com o servidor, tente novamente.");
    }

    @Override
    public void onPickerCanceled() {
        binding.inputSize.getEditText().requestFocus();
    }
}
