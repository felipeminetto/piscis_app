package fsm.com.piscis.fish.edit;

import android.util.Log;

import fsm.com.piscis.base.BasePresenter;
import fsm.com.piscis.domain.APIService;
import fsm.com.piscis.domain.Utils;
import fsm.com.piscis.domain.contracts.FishEditContract;
import fsm.com.piscis.model.Fish;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

import static android.content.ContentValues.TAG;

public class FishEditPresenter extends BasePresenter<FishEditContract.View> implements FishEditContract.Presenter {

    public FishEditPresenter(APIService apiService) {
        super(apiService);
    }

    @Override
    public void editFish(Fish fish) {
        addSubscription(apiService.editFish(fish)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(uFish -> {
                    ifViewAttached(FishEditContract.View::fishEditSuccess);
                }, throwable -> {
                    ifViewAttached(FishEditContract.View::hideProgress);
                    if (throwable instanceof HttpException) {
                        if (((HttpException) throwable).code() == 401) {
                            ifViewAttached(view -> view.logout());
                            ;
                        } else if (((HttpException) throwable).code() >= 500) {
                            ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                        } else {
                            String message = Utils.getErrorMessage(throwable);
                            ifViewAttached(view -> view.showError(message));
                        }
                    } else {
                        ifViewAttached(view -> view.showError("Ocorreu um erro inesperado."));
                    }
                    Log.e(TAG, "loadFishes: ", throwable);
                }));
    }
}
