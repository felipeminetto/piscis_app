package fsm.com.piscis.fish.list;

import android.util.Log;

import fsm.com.piscis.base.BasePresenter;
import fsm.com.piscis.domain.APIService;
import fsm.com.piscis.domain.Utils;
import fsm.com.piscis.domain.contracts.FishContract;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

import static android.content.ContentValues.TAG;

public class FishPresenter extends BasePresenter<FishContract.View> implements FishContract.Presenter {

    public FishPresenter(APIService apiService) {
        super(apiService);
    }

    @Override
    public void loadFishes() {
        ifViewAttached(FishContract.View::showProgress);
        addSubscription(apiService.getAllFishes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(fishList -> {
                            ifViewAttached(view -> view.showFishes(fishList));
                            ifViewAttached(FishContract.View::hideProgress);
                        },
                        throwable -> {
                            ifViewAttached(FishContract.View::hideProgress);
                            if (throwable instanceof HttpException) {
                                if (((HttpException) throwable).code() == 401) {
                                    ifViewAttached(FishContract.View::logout);
                                } else if (((HttpException) throwable).code() >= 500) {
                                    ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                                } else {
                                    String message = Utils.getErrorMessage(throwable);
                                    ifViewAttached(view -> view.showError(message));
                                }
                            } else {
                                ifViewAttached(view -> view.showError("Ocorreu um erro inesperado."));
                            }
                            Log.e(TAG, "loadFishes: ", throwable);
                        })
        );
    }

    @Override
    public void findFish(String fishInfo) {
        ifViewAttached(FishContract.View::showProgress);
        addSubscription(apiService.findFish(fishInfo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(fishList -> {
                            if (fishList.size() == 1) {
                                ifViewAttached(view -> view.showFishDetail(fishList.get(0)));
                            } else {
                                ifViewAttached(view -> view.showFishes(fishList));
                            }
                            ifViewAttached(FishContract.View::hideProgress);
                        },
                        throwable -> {
                            ifViewAttached(FishContract.View::hideProgress);
                            if (throwable instanceof HttpException) {
                                if (((HttpException) throwable).code() == 401) {
                                    ifViewAttached(view -> view.logout());
                                } else if (((HttpException) throwable).code() >= 500) {
                                    ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                                } else {
                                    String message = Utils.getErrorMessage(throwable);
                                    ifViewAttached(view -> view.showError(message));
                                }
                            } else {
                                ifViewAttached(view -> view.showError("Ocorreu um erro inesperado."));
                            }
                            Log.e(TAG, "loadFishes: ", throwable);
                        })
        );
    }
}
