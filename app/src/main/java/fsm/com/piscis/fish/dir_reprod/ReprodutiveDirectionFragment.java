package fsm.com.piscis.fish.dir_reprod;

import android.app.AlertDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import fsm.com.piscis.PiscisApplication;
import fsm.com.piscis.R;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.databinding.ListFragmentBinding;
import fsm.com.piscis.domain.contracts.ReprodutiveDirectionContract;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.model.KinshipLevel;
import fsm.com.piscis.fish.detail.adapter.FishDetailAdapter;
import fsm.com.piscis.domain.contracts.SelectionListener;
import fsm.com.piscis.domain.contracts.NavigationListener;
import fsm.com.piscis.tank.detail.TankDetailFragment;

public class ReprodutiveDirectionFragment extends android.support.v4.app.Fragment implements ReprodutiveDirectionContract.View, SelectionListener {

    private ListFragmentBinding binding;
    private ReprodutiveDirectionPresenter presenter;
    private Fish fish;
    private NavigationListener listener;

    public static ReprodutiveDirectionFragment newInstance(Fish fish) {
        Bundle args = new Bundle();

        ReprodutiveDirectionFragment fragment = new ReprodutiveDirectionFragment();
        args.putParcelable(Constants.FISH, fish);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false);
        fish = getArguments().getParcelable(Constants.FISH);
        binding.rvItems.setLayoutManager(new LinearLayoutManager(getActivity()));
        presenter = new ReprodutiveDirectionPresenter(((PiscisApplication) getActivity().getApplication()).getApi());
        presenter.attachView(this);
        presenter.loadMatchesForFish(fish.getId());

        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            binding.swipeRefreshLayout.setRefreshing(false);
            presenter.loadMatchesForFish(fish.getId());
        });

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Direcionamento de cruzamento");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showMatchFishes(List<KinshipLevel> fishes) {
        if (fishes.isEmpty()) {
            binding.rvItems.setVisibility(View.INVISIBLE);
            binding.txvNoContent.setVisibility(View.VISIBLE);
        } else {
            binding.rvItems.setAdapter(new FishDetailAdapter(fishes, fish, this));
            binding.txvNoContent.setVisibility(View.GONE);
            binding.rvItems.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showServerError() {
        new AlertDialog.Builder(getActivity())
                .setPositiveButton(android.R.string.ok, null)
                .setMessage("Houve algum erro na comunicação com o servidor, tente novamente.")
                .setTitle("Erro")
                .create().show();
    }

    @Override
    public void showProgress() {
        binding.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.progress.setVisibility(View.GONE);
    }

    @Override
    public void selectedFish(Fish fish) {
        if (fish.getTank() != null) {
            TankDetailFragment fragment = TankDetailFragment.newInstance(fish.getTank());
            fragment.setListener(listener);
            listener.addFragmentToNavigation(fragment, Constants.FISH_DETAIL);
        } else {
            Toast.makeText(getContext(), "Esse peixe não está inserido em nenhum tanque.", Toast.LENGTH_SHORT).show();
        }
    }

    public void setListener(NavigationListener listener) {
        this.listener = listener;
    }
}
