package fsm.com.piscis.fish.list.adapter;

import android.support.v7.widget.RecyclerView;

import fsm.com.piscis.databinding.FishItemBinding;
import fsm.com.piscis.domain.contracts.SelectionListener;
import fsm.com.piscis.model.Fish;

public class FishViewHolder extends RecyclerView.ViewHolder {

    private FishItemBinding binding;

    public FishViewHolder(FishItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(Fish fish, SelectionListener selectionListener) {
        binding.setFish(fish);
        binding.getRoot().setOnClickListener(view -> {
            selectionListener.selectedFish(fish);
        });

        binding.executePendingBindings();
    }
}
