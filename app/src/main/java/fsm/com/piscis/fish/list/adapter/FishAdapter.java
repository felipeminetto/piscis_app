package fsm.com.piscis.fish.list.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import fsm.com.piscis.R;
import fsm.com.piscis.databinding.FishItemBinding;
import fsm.com.piscis.domain.contracts.SelectionListener;
import fsm.com.piscis.model.Fish;

public class FishAdapter extends RecyclerView.Adapter<FishViewHolder> {

    private List<Fish> fishes;
    private SelectionListener selectionListener;

    public FishAdapter(List<Fish> fishes, SelectionListener selectionListener) {
        this.fishes = fishes;
        this.selectionListener = selectionListener;
    }

    @NonNull
    @Override
    public FishViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FishItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.fish_item, parent, false);
        return new FishViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FishViewHolder holder, int position) {
        holder.bind(fishes.get(position), selectionListener);
    }

    @Override
    public int getItemCount() {
        return fishes.size();
    }
}
