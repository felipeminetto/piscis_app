package fsm.com.piscis.fish.edit;


import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import fsm.com.piscis.PiscisApplication;
import fsm.com.piscis.R;
import fsm.com.piscis.base.Constants;
import fsm.com.piscis.databinding.FishEditFragmentBinding;
import fsm.com.piscis.domain.contracts.FishEditContract;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.domain.contracts.NavigationListener;
import fsm.com.piscis.login.LoginActivity;
import fsm.com.piscis.utils.MyDatePicker;

import static android.content.Context.MODE_PRIVATE;

public class FishEditFragment extends Fragment implements FishEditContract.View, MyDatePicker.DateSelectedListener {

    private Fish fish;
    private FishEditFragmentBinding binding;
    private Date selectedDate;
    private NavigationListener listener;
    private FishEditPresenter presenter;

    public static FishEditFragment newInstance(Fish fish) {
        FishEditFragment fragment = new FishEditFragment();
        Bundle args = new Bundle();
        args.putParcelable(Constants.FISH, fish);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fish = getArguments().getParcelable(Constants.FISH);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fish_edit_fragment, container, false);
        presenter = new FishEditPresenter(((PiscisApplication) getActivity().getApplication()).getApi());
        presenter.attachView(this);
        binding.setFish(fish);
        binding.setFragment(this);
        binding.edtDateCapture.setShowSoftInputOnFocus(false);
        binding.edtDateCapture.setOnFocusChangeListener((view, b) -> {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            if (b) showDatePicker();
        });

        return binding.getRoot();
    }

    private void showDatePicker() {
        MyDatePicker datePicker = new MyDatePicker();
        datePicker.setListener(this);
        datePicker.show(getFragmentManager(), "date_picker");
    }

    @Override
    public void onPickerCanceled() {
        binding.edtPlaceCapture.requestFocus();
    }

    @Override
    public void dateSelected(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date d = new Date(calendar.getTimeInMillis());
        selectedDate = d;
        DateFormat dateFormat = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT, Locale.forLanguageTag("pt-BR"));
        binding.edtDateCapture.setText(dateFormat.format(d));
        binding.edtPlaceCapture.requestFocus();
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }

    public void cancelEdit() {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção")
                .setMessage("Tem certeza que deseja cancelar a edição deste peixe?")
                .setPositiveButton("Sim", (dialogInterface, i) -> {
                    listener.finishFragment();
                })
                .setNegativeButton(android.R.string.no, null)
                .create().show();
    }

    public void saveFish() {
        fish.setLabId(binding.edtLabId.getText().toString());
        fish.setSpecies(binding.edtSpecies.getText().toString());
        fish.setDateCapture(selectedDate);
        fish.setLocality(binding.edtPlaceCapture.getText().toString());
        fish.setSize(Integer.parseInt(binding.edtSize.getText().toString()));
        fish.setWeight(Float.parseFloat(binding.edtWeight.getText().toString().replace(',', '.')));
        fish.setGender(binding.spnGender.getSelectedItemPosition());
        fish.setObservation(binding.edtObs.getText().toString());

        presenter.editFish(fish);
    }

    @Override
    public void showError(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Atenção!").setMessage(message).create().show();
    }

    @Override
    public void showServerError() {
        new AlertDialog.Builder(getContext())
                .setPositiveButton(android.R.string.ok, null)
                .setMessage("Houve algum erro na comunicação com o servidor, tente novamente.")
                .setTitle("Erro")
                .create().show();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Editar peixe");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void fishEditSuccess() {

        new AlertDialog.Builder(getContext())
                .setTitle("Sucesso!")
                .setMessage("Peixe alterado com sucesso!")
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                    listener.finishFragment();
                })
                .create().show();
    }

    public void setListener(NavigationListener listener) {
        this.listener = listener;
    }

    public void cancelEditForNavigation(int menuItemId) {
        new AlertDialog.Builder(getContext())
                .setTitle("Atenção")
                .setMessage("Tem certeza que deseja cancelar a edição deste peixe?")
                .setPositiveButton("Sim", (dialogInterface, i) -> {
                    listener.navigateTo(menuItemId);
                })
                .setNegativeButton(android.R.string.no, null)
                .create().show();
    }

    @Override
    public void logout() {
        new android.support.v7.app.AlertDialog.Builder(getContext())
                .setTitle("Atenção!")
                .setMessage("A sua secção expirou! Favor efetuar o login novamente.")
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                    SharedPreferences prefs = getContext().getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
                    prefs.edit().clear().apply();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                    getActivity().finish();
                })
                .create().show();
    }
}
