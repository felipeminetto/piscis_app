package fsm.com.piscis.fish.detail;

import java.util.Date;

import fsm.com.piscis.base.BasePresenter;
import fsm.com.piscis.domain.APIService;
import fsm.com.piscis.domain.Utils;
import fsm.com.piscis.domain.contracts.FishDetailContract;
import fsm.com.piscis.model.Fish;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class FishDetailPresenter extends BasePresenter<FishDetailContract.View> implements FishDetailContract.Presenter {

    public FishDetailPresenter(APIService apiService) {
        super(apiService);
    }

    @Override
    public void addReproductionDate(Date d, Fish fish) {
        fish.getReprodutiveHistory().add(d);
        addSubscription(apiService.editFish(fish)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(fish1 -> ifViewAttached(view -> {
                    view.updateFish(fish1);
                    view.showSuccess("Data de reprodução adicionada com sucesso!");
                }), throwable -> {
                    if (throwable instanceof HttpException) {
                        if (((HttpException) throwable).code() == 401) {
                            ifViewAttached(view -> view.logout());
                            ;
                        } else if (((HttpException) throwable).code() >= 500) {
                            ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                        } else {
                            String message = Utils.getErrorMessage(throwable);
                            ifViewAttached(view -> view.showError(message));
                        }
                    } else {
                        ifViewAttached(view -> view.showError("Não foi possível adicionar a data de reprodução. Verifique sua conexão com a internet e tente novamente mais tarde."));
                    }
                }));
    }

    @Override
    public void discardFish(Fish fish) {
        fish.setDiscarded(true);
        addSubscription(apiService.editFish(fish)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(fish1 -> ifViewAttached(view -> {
                    view.updateFish(fish1);
                    view.showSuccess("Peixe descartado com sucesso!");
                }), throwable -> {

                    if (throwable instanceof HttpException) {
                        if (((HttpException) throwable).code() == 401) {
                            ifViewAttached(view -> view.logout());
                            ;
                        } else if (((HttpException) throwable).code() >= 500) {
                            ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                        } else {
                            String message = Utils.getErrorMessage(throwable);
                            ifViewAttached(view -> view.showError(message));
                        }
                    } else {
                        ifViewAttached(view -> view.showError("Não foi possível adicionar a data de reprodução. Verifique sua conexão com a internet e tente novamente mais tarde."));
                    }
                }));
    }
}
