package fsm.com.piscis.fish.detail.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import fsm.com.piscis.R;
import fsm.com.piscis.databinding.FishCrossingItemBinding;
import fsm.com.piscis.model.Fish;
import fsm.com.piscis.model.KinshipLevel;
import fsm.com.piscis.domain.contracts.SelectionListener;

public class FishDetailAdapter extends RecyclerView.Adapter<FishDetailViewHolder> {

    private List<KinshipLevel> matches;
    private Fish fish;
    private SelectionListener selectionListener;

    public FishDetailAdapter(List<KinshipLevel> matches, Fish fish, SelectionListener selectionListener) {
        this.matches = matches;
        this.fish = fish;
        this.selectionListener = selectionListener;
    }

    @NonNull
    @Override
    public FishDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FishCrossingItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.fish_crossing_item, parent, false);
        return new FishDetailViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FishDetailViewHolder holder, int position) {
        holder.bind(matches.get(position), fish, selectionListener);
    }

    @Override
    public int getItemCount() {
        return matches.size();
    }
}
