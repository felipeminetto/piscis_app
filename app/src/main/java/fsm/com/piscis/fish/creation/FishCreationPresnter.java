package fsm.com.piscis.fish.creation;

import android.util.Log;

import fsm.com.piscis.base.BasePresenter;
import fsm.com.piscis.domain.APIService;
import fsm.com.piscis.domain.Utils;
import fsm.com.piscis.domain.contracts.FishCreationContract;
import fsm.com.piscis.model.Fish;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class FishCreationPresnter extends BasePresenter<FishCreationContract.View> implements FishCreationContract.Presenter {

    public FishCreationPresnter(APIService apiService) {
        super(apiService);
    }

    @Override
    public void saveFish(Fish fish) {
        if (fish.getTag() != null && !fish.getTag().isEmpty()) {
            createFish(fish, false);
        } else {
            ifViewAttached(view -> view.showTagWarning(fish));
        }
    }

    @Override
    public void createFish(Fish fish, boolean discardAndSave) {
        if (discardAndSave) {
            addSubscription(apiService.discardFish(fish.getId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> createFish(fish, false), throwable -> {
                        if (throwable instanceof HttpException) {
                            if (((HttpException) throwable).code() == 401) {
                                ifViewAttached(view -> view.logout());
                                ;
                            } else if (((HttpException) throwable).code() >= 500) {
                                ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                            } else {
                                String message = Utils.getErrorMessage(throwable);
                                ifViewAttached(view -> view.showError(message));
                            }
                        } else {
                            ifViewAttached(view -> view.showError("Ocorreu um erro inesperado."));
                        }
                    }));
        } else {
            addSubscription(apiService.createFish(fish)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(responseFish -> ifViewAttached(FishCreationContract.View::fishSaved), throwable -> {
                        if (throwable instanceof HttpException) {
                            if (((HttpException) throwable).code() == 401) {
                                ifViewAttached(view -> view.logout());
                                ;
                            } else if (((HttpException) throwable).code() >= 500) {
                                ifViewAttached(view -> view.showError("Houve um erro com o servidor, verifique sua conexão e tente novamente."));
                            } else {
                                if (((HttpException) throwable).code() == 409) {
                                    String message = Utils.getErrorMessage(throwable) + "\nDeseja descartar o peixe e cadastrar o novo peixe com essa tag?";
                                    ifViewAttached(view -> view.showTagError(fish, message));
                                } else {
                                    String message = Utils.getErrorMessage(throwable);
                                    if (!message.isEmpty())
                                        ifViewAttached(view -> view.showError(message));
                                    else
                                        ifViewAttached(view -> view.showError("Não foi possível cadastrar o peixe, tente novamente mais tarde."));
                                }
                            }
                        }
                        Log.e("asdfasdfasdf", "createFish: error creating fish");
                        Log.e("asdfasdfasdf", "createFish: " + throwable.getMessage());
                    }));
        }
    }
}
